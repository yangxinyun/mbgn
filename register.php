<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>用户注册</title>
    <link rel="stylesheet" href="static/lib/layui/css/layui.css">
    <link href="static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="static/css/register.css">
</head>

<body>
    <div class="dowebok">
        <div class="logo"></div>
        <div class="form-item">
            <input id="username" type="text" autocomplete="off" placeholder="用户名"><i class="iconfont icon-xingmingyonghumingnicheng"></i>
        </div>
        <div class="form-item">
            <input type="text" id="nick" placeholder="昵称" name="nick"><i class="iconfont icon-nicheng"></i>
        </div>
        <div class="form-item">
            <input id="password" type="password" autocomplete="off" placeholder="登录密码"><i class="iconfont icon-mima"></i>
        </div>
        <div class="form-item">
            <input type="password" id="repassword" placeholder="确认密码" name="repassword"><i class="iconfont icon-mima"></i>
        </div>
        <div class="form-item">
            <input id="authcode" type="text" name='authcode' autocomplete="off" placeholder="验证码"><i class="iconfont icon-ecurityCode"></i>
            <p class="tip">验证码不正确</p>
            <div class="codeimg">
                <img id="captcha_img" src='public/captcha.php?r=echo rand(); ?>' style="width:100px; height:30px" />
                <a style="color:#fff" href="#" id="change">换一个?</a>
            </div>
        </div>
        <div class="form-item"><button id="submit">登 录</button></div>
        <div class="reg-bar">
            <a class="reg" href="./index.php">已有账号？去登陆</a>
        </div>
    </div>
    <script src="static/lib/jquery/jquery.min.js"></script>
    <script src="static/lib/layui/layui.all.js"></script>
    <script>
        $(function() {
            var layer = layui.layer
            $("#change").click(function(e) {
                e.preventDefault();
                $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
            })
            $('#submit').click(function() {
                var username = $('#username').val(),
                    password = $('#password').val(),
                    repassword = $('#repassword').val(),
                    nick = $('#nick').val(),
                    authcode = $('#authcode').val();
                if (username == '' || username.length <= 0) {
                    layer.tips('用户名不能为空', '#username', {
                        time: 2000,
                        tips: 2
                    });
                    $('#username').focus();
                    return false;
                }
                if (username.length < 6 || username.length > 16) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('用户名长度必须为6到16位字符', '#username', {
                        time: 2000,
                        tips: 2
                    });
                    $('#username').focus();
                    return false;
                }

                if (nick == '' || nick.length <= 0) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('昵称不能为空', '#nick', {
                        time: 2000,
                        tips: 2
                    });
                    $('#nick').focus();
                    return false;
                }
                if (nick.length < 2 || nick.length > 10) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('昵称长度必须为2到10位字符', '#nick', {
                        time: 2000,
                        tips: 2
                    });
                    $('#nick').focus();
                    return false;
                }

                if (password == '' || password.length <= 0) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('密码不能为空', '#password', {
                        time: 2000,
                        tips: 2
                    });
                    $('#password').focus();
                    return false;
                }

                if (password.length < 6 || password.length > 16) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('密码长度必须为6到16位字符', '#password', {
                        time: 2000,
                        tips: 2
                    });
                    $('#password').focus();
                    return false;
                }

                if (repassword == '' || repassword.length <= 0 || (password != repassword)) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('两次密码输入不一致', '#repassword', {
                        time: 2000,
                        tips: 2
                    });
                    $('#repassword').focus();
                    return false;
                }

                if (repassword.length < 6 || repassword.length > 16) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('密码长度必须为6到16位字符', '#password', {
                        time: 2000,
                        tips: 2
                    });
                    $('#password').focus();
                    return false;
                }

                if (authcode.length == '' || authcode.length <= 0) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('验证码长度必须为4位', '#authcode', {
                        time: 2000,
                        tips: 4
                    });
                    $('#authcode').focus();
                    return false;
                }

                if (authcode.length != 4) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('验证码长度必须为4位', '#authcode', {
                        time: 2000,
                        tips: 4
                    });
                    $('#authcode').focus();
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "model/login/register.php",
                    data: {
                        username: $("#username").val(),
                        password: $("#password").val(),
                        authcode: $("#authcode").val(),
                        nick: $("#nick").val(),
                        repassword: $("#repassword").val()
                    },
                    dataType: "json",
                    success: function(data) {
                        console.log(data)
                        if (data.code == 0) {
                            $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                            layer.tips('验证码错误', '#authcode', {
                                time: 2000,
                                tips: 4
                            });
                        } else if (data.code == 1) {
                            $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                            layer.tips('用户名已被注册', '#username', {
                                time: 2000,
                                tips: 2
                            });
                        } else if (data.code == 2) {
                            layer.confirm('恭喜您！注册成功！', {
                                btn: ['去登录', '关闭'] //按钮
                            }, function() {
                                window.location = "index.php"
                            }, function() {});
                        } else if (data.code == 3) {
                            $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                            layer.tips('昵称已被注册', '#nick', {
                                time: 2000,
                                tips: 2
                            });
                        } else {
                            layer.alert('注册失败！，再试一次！');
                        }
                    }
                })
                return true;
            })
        })
    </script>
</body>

</html>