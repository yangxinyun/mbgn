<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>用户登录</title>
    <link rel="stylesheet" href="static/lib/layui/css/layui.css">
    <link rel="stylesheet" href="static/css/login.css">
</head>

<body>
    <div class="dowebok">
        <div class="logo"></div>
        <div class="form-item">
            <input id="username" type="text" autocomplete="off" placeholder="用户名"><i class="iconfont icon-xingmingyonghumingnicheng"></i>
            <p class="tip">请输入合法的用户名</p>
        </div>
        <div class="form-item">
            <input id="password" type="password" autocomplete="off" placeholder="登录密码"><i class="iconfont icon-mima"></i>
            <p class="tip">用户名或密码不正确</p>
        </div>
        <div class="form-item">
            <input id="authcode" type="text" name='authcode' autocomplete="off" placeholder="验证码"><i class="iconfont icon-ecurityCode"></i>
            <p class="tip">验证码不正确</p>
            <div class="codeimg">
                <img id="captcha_img" border='1' src='public/captcha.php?r=echo rand(); ?>' style="width:100px; height:30px" />
                <a style="color:#fff" href="#" id="change">换一个?</a>
            </div>
        </div>
        <div class="form-item"><button id="submit">登 录</button></div>
        <div class="reg-bar">
            <a class="reg" href="./register.php">没有账号？去注册</a>
        </div>
    </div>
    <script src="static/lib/jquery/jquery.min.js"></script>
    <script src="static/lib/layui/layui.all.js"></script>
    <script>
        $(function() {
            $("#change").click(function(e) {
                e.preventDefault();
                $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
            })
            var layer = layui.layer

            $("#username").blur(function(){
                $.ajax({
                    type:"POST",
                    url: "model/user/avatar.php",
                    data:{
                        username:$(this).val()
                    },
                    dataType: "json",
                    success:function(data){
                        if(data.avatar){
                            data.avatar = data.avatar.replace("../","")
                            $(".logo").css({
                                "background":"url(" + data.avatar + ") 0 0 no-repeat",
                                "background-size":"contain"
                            });
                        }  
                        else{
                            $(".logo").css("background","url(static/img/login.png) 0 0 no-repeat");
                        }
                    }
                })
            })


            $('#submit').click(function() {
                var username = $('#username').val(),
                    password = $('#password').val(),
                    authcode = $('#authcode').val();
                if (username == '' || username.length <= 0) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('用户名不能为空', '#username', {
                        time: 2000,
                        tips: 2
                    });
                    $('#username').focus();
                    return false;
                }
                if (username.length < 6 || username.length > 16) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('用户名长度必须为6到16位字符', '#username', {
                        time: 2000,
                        tips: 2
                    });
                    $('#username').focus();
                    return false;
                }
                if (password == '' || password.length <= 0) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('密码不能为空', '#password', {
                        time: 2000,
                        tips: 2
                    });
                    $('#password').focus();
                    return false;
                }
                if (password.length < 6 || password.length > 16) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('密码长度必须为6到16位字符', '#password', {
                        time: 2000,
                        tips: 2
                    });
                    $('#password').focus();
                    return false;
                }
                if (authcode.length == '' || authcode.length <= 0) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('验证码长度必须为4位', '#authcode', {
                        time: 2000,
                        tips: 4
                    });
                    $('#authcode').focus();
                    return false;
                }

                if (authcode.length != 4) {
                    $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                    layer.tips('验证码长度必须为4位', '#authcode', {
                        time: 2000,
                        tips: 4
                    });
                    $('#authcode').focus();
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "model/login/login.php",
                    data: {
                        username: $("#username").val(),
                        password: $("#password").val(),
                        authcode: $("#authcode").val(),
                    },
                    dataType: "json",
                    success: function(data) {
                        console.log(data)
                        if (data.code == 0) {
                            $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                            layer.tips('验证码错误', '#authcode', {
                                time: 2000,
                                tips: 4
                            });
                        } else if (data.code == 1) {
                            $("#captcha_img").attr("src", 'public/captcha.php?r=' + Math.random());
                            layer.tips('用户名或者密码错误', '#username', {
                                time: 2000,
                                tips: 2
                            });
                        } else if (data.code == 2) {
                            window.location = "views/index/index.php";
                        } else {
                            layer.alert('服务器异常');
                        }
                    }
                })
                return true;
            })
        })
    </script>
</body>

</html>