/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mbgn

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-01-06 10:11:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for history
-- ----------------------------
DROP TABLE IF EXISTS `history`;
CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) NOT NULL,
  `nick` varchar(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `crud` varchar(10) NOT NULL,
  `pc_url` varchar(100) DEFAULT NULL,
  `mobile_url` varchar(255) DEFAULT NULL,
  `content_url` varchar(100) DEFAULT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1019 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of history
-- ----------------------------
INSERT INTO `history` VALUES ('14', '127.0.0.1', '杨鑫', '东方新闻网', 'www.hiphopcc.com', null, '增加', null, null, null, '2019-05-17 09:09:14');
INSERT INTO `history` VALUES ('15', '127.0.0.1', '杨鑫', '看奇闻', 'www. cyxlyz.com', null, '增加', null, null, null, '2019-05-17 09:10:30');
INSERT INTO `history` VALUES ('16', '127.0.0.1', '杨鑫', '云梦比特币新闻网', ' gaojige.com', null, '增加', null, null, null, '2019-05-17 09:11:27');
INSERT INTO `history` VALUES ('17', '127.0.0.1', '杨鑫', '孝感娱乐资讯网', 'http://ihuolong.com', null, '增加', null, null, null, '2019-05-17 09:12:13');
INSERT INTO `history` VALUES ('19', '127.0.0.1', '杨鑫', '硚口文章网', '0471dy.com', null, '增加', null, null, null, '2019-05-17 09:13:40');
INSERT INTO `history` VALUES ('20', '127.0.0.1', '杨鑫', '看奇闻', 'www.cyxlyz.com', null, '编辑', null, null, null, '2019-05-17 09:13:57');
INSERT INTO `history` VALUES ('21', '127.0.0.1', '杨鑫', '新县文章网', 'jlsttyy.com', null, '增加', null, null, null, '2019-05-17 09:14:40');
INSERT INTO `history` VALUES ('22', '127.0.0.1', '杨鑫', '蔡甸律师事务所', 'china-awesome.com', null, '编辑', null, null, null, '2019-05-17 09:14:50');
INSERT INTO `history` VALUES ('23', '127.0.0.1', '杨鑫', '孝感娱乐资讯网', 'ihuolong.com', null, '编辑', null, null, null, '2019-05-17 09:15:03');
INSERT INTO `history` VALUES ('24', '127.0.0.1', '杨鑫', '洪山新闻网', 'jhljzz.com', null, '增加', null, null, null, '2019-05-17 09:15:45');
INSERT INTO `history` VALUES ('25', '127.0.0.1', '杨鑫', '光谷创业IT资讯网', '17ixue.com', null, '增加', null, null, null, '2019-05-17 09:16:40');
INSERT INTO `history` VALUES ('26', '127.0.0.1', '杨鑫', '云梦比特币新闻网', 'www.gaojige.com', null, '编辑', null, null, null, '2019-05-17 09:16:52');
INSERT INTO `history` VALUES ('27', '127.0.0.1', '杨鑫', '孝感娱乐资讯网', 'www.ihuolong.com', null, '编辑', null, null, null, '2019-05-17 09:16:58');
INSERT INTO `history` VALUES ('28', '127.0.0.1', '杨鑫', '蔡甸律师事务所', 'www.china-awesome.com', null, '编辑', null, null, null, '2019-05-17 09:17:05');
INSERT INTO `history` VALUES ('29', '127.0.0.1', '杨鑫', '硚口文章网', 'www.0471dy.com', null, '编辑', null, null, null, '2019-05-17 09:17:11');
INSERT INTO `history` VALUES ('30', '127.0.0.1', '杨鑫', '硚口文章网', 'www.0471dy.com', null, '编辑', null, null, null, '2019-05-17 09:17:14');
INSERT INTO `history` VALUES ('31', '127.0.0.1', '杨鑫', '新县文章网', 'www.jlsttyy.com', null, '编辑', null, null, null, '2019-05-17 09:17:17');
INSERT INTO `history` VALUES ('32', '127.0.0.1', '杨鑫', '洪山新闻网', 'www.jhljzz.com', null, '编辑', null, null, null, '2019-05-17 09:17:21');
INSERT INTO `history` VALUES ('33', '127.0.0.1', '杨鑫', '光谷创业IT资讯网', 'www.17ixue.com', null, '编辑', null, null, null, '2019-05-17 09:17:27');
INSERT INTO `history` VALUES ('34', '127.0.0.1', '杨鑫', '关山手机资讯网', 'www.repoequipmentsales.com', null, '增加', null, null, null, '2019-05-17 09:19:45');
INSERT INTO `history` VALUES ('35', '127.0.0.1', '杨鑫', '黄陂文章网', 'www.casaperferiebetania.com', null, '增加', null, null, null, '2019-05-17 09:20:41');
INSERT INTO `history` VALUES ('36', '127.0.0.1', '杨鑫', '阳逻文章网', 'www.robustfastlock.com', null, '增加', null, null, null, '2019-05-17 09:23:30');
INSERT INTO `history` VALUES ('37', '127.0.0.1', '杨鑫', '江岸新闻网', 'www.katellachurch.com', null, '增加', null, null, null, '2019-05-17 09:24:17');
INSERT INTO `history` VALUES ('38', '127.0.0.1', '杨鑫', '东西湖新闻网', 'www.wirelesscompanys.com', null, '增加', null, null, null, '2019-05-17 09:26:19');
INSERT INTO `history` VALUES ('39', '127.0.0.1', '杨鑫', '汉口新闻网', 'www.aaroneye.com', null, '增加', null, null, null, '2019-05-17 09:27:07');
INSERT INTO `history` VALUES ('40', '127.0.0.1', '杨鑫', '古田个性签名网', 'www.xzhgw.com', null, '增加', null, null, null, '2019-05-17 09:27:50');
INSERT INTO `history` VALUES ('41', '127.0.0.1', '杨鑫', '东湖科技资讯', 'http://videoclipsdump.com', null, '增加', null, null, null, '2019-05-17 09:28:28');
INSERT INTO `history` VALUES ('42', '127.0.0.1', '杨鑫', '东湖科技资讯', 'www.videoclipsdump.com', null, '编辑', null, null, null, '2019-05-17 09:28:33');
INSERT INTO `history` VALUES ('52', '127.0.0.1', '杨鑫', '龙岗汽车网', 'www.sasgeobat.com', null, '编辑', null, null, null, '2019-06-03 09:56:31');
INSERT INTO `history` VALUES ('53', '127.0.0.1', '杨鑫', '蛇口教育资讯网', 'http://www.acxf.net/', null, '增加', null, null, null, '2019-06-03 09:57:41');
INSERT INTO `history` VALUES ('54', '127.0.0.1', '杨鑫', '蛇口教育资讯网', 'http://www.f-vs.net', null, '编辑', null, null, null, '2019-06-03 09:58:14');
INSERT INTO `history` VALUES ('55', '127.0.0.1', '杨鑫', '团风新闻网', 'http://www.acxf.net/', null, '增加', null, null, null, '2019-06-03 10:00:23');
INSERT INTO `history` VALUES ('56', '192.168.10.41', '杨鑫', '衡水时尚网', 'www.gdcml.com', null, '增加', null, null, null, '2019-06-10 14:18:45');
INSERT INTO `history` VALUES ('57', '192.168.10.41', '杨鑫', '常德图片网', 'http://www.zanzicrea.com/', null, '增加', null, null, null, '2019-06-10 14:19:12');
INSERT INTO `history` VALUES ('58', '192.168.10.41', '杨鑫', '新乡动漫资讯网', 'http://www.rohm-ic.com/', null, '增加', null, null, null, '2019-06-10 14:19:38');
INSERT INTO `history` VALUES ('59', '192.168.10.41', '杨鑫', '红安物联网', 'http://www.jingsefushi.com/', null, '增加', null, null, null, '2019-06-10 14:20:09');
INSERT INTO `history` VALUES ('60', '192.168.10.41', '杨鑫', '通城房产资讯网', 'http://tiger-fy.com', null, '增加', null, null, null, '2019-06-10 14:21:07');
INSERT INTO `history` VALUES ('61', '127.0.0.1', 'admin888', '通城房产资讯网', 'http://tiger-fy.com', null, '编辑', '', null, '', '2019-09-06 18:39:15');
INSERT INTO `history` VALUES ('865', '127.0.0.1', 'admin888', '第一预测网', 'www.ly-u.com', '星座、生肖、风水', '增加', 'https://www.d1xz.net', 'https://3g.d1xz.net', 'https://www.d1xz.net', '2019-09-07 09:30:01');
INSERT INTO `history` VALUES ('866', '127.0.0.1', 'admin888', '萌宠生活馆', 'www.454950.net', '宠物', '增加', 'http://www.chongbaba.com/', 'https://m.ichong123.com/', 'http://www.sohu.com/', '2019-09-07 09:30:01');
INSERT INTO `history` VALUES ('867', '127.0.0.1', 'admin888', '人人教育网', 'http://www.ie-ter.com', '教育', '增加', 'http://www.jyb.cn/', 'https://mip.5djiaren.com/', 'http://news.e21.cn/  http://news.haedu.cn', '2019-09-07 09:30:01');
INSERT INTO `history` VALUES ('868', '127.0.0.1', 'admin888', '游知有戏', 'www.sjzfgl.com', '游戏', '增加', 'http://www.ali213.net/news/', 'http://3g.ali213.net/news/', 'http://www.doyo.cn', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('869', '127.0.0.1', 'admin888', '衣搭时尚', 'www.rrydss.com', '衣服、护肤、娱乐', '增加', 'http://www.mimito.com.cn/', '响应式', 'http://www.7624.net/  http://www.mimito.com.cn', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('870', '127.0.0.1', 'admin888', '素食天下', 'www.grtd.net', '养生、素食', '增加', 'http://www.ssysw.cn/', 'http://m.yuexw.com/', 'http://www.chinavegan.com', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('871', '127.0.0.1', 'admin888', '爱车有道网', 'www.hlmusicworld.com', '汽车', '增加', 'http://www.chinaautonews.com.cn/', 'http://www.html5code.net/mobile/plugin-3395.html', 'https://www.autotimes.com.cn', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('872', '127.0.0.1', 'admin888', '以食养身', 'www.ccjlgg.com', '美食、养生', '增加', 'http://www.ttys5.com/', 'http://www.html5code.net/mobile/plugin-3393.html', 'http://www.lzyysw.com', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('873', '127.0.0.1', 'admin888', '天天保健', 'http://www.dotanb.com/', '保健，按摩', '增加', 'http://www.cayb.net/', '响应式', 'http://care.39.net/dzbj/baby/', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('874', '127.0.0.1', 'admin888', '二次元网', 'www.soyhw.com', '动漫', '增加', 'http://www.005.tv/', '响应式', 'http://acg.gamersky.com/pic/cosplay/', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('875', '127.0.0.1', 'admin888', '友鼎金融网', 'www.dy-tm.com', '金融', '增加', 'http://www.ahjr.com.cn/', '响应式', 'http://finance.fjndwb.com/', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('876', '127.0.0.1', 'admin888', '荣星数码', 'www.ituye.com', '数码产品', '增加', 'http://www.hsw.cn/', 'http://m.dtphoto.com/sj/2018/12/11/qfdmopap.html', 'http://digi.hsw.cn/smjd/', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('877', '127.0.0.1', 'admin888', '兴民农业资讯', 'www.zjkyqb.com', '农业', '增加', 'http://www.sjwz.cn/', 'http://meirongbjw.yilianapp.com/', 'care.39.net/ys/shxg/', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('878', '127.0.0.1', 'admin888', '潮， 范儿', 'www.1982y.cn', '潮流，服装，鞋', '增加', 'http://www.azs.org.cn/', 'http://m.china-ef.com/', 'http://www.cnxz.cn/news/category-10-1.html', '2019-09-07 09:30:02');
INSERT INTO `history` VALUES ('879', '127.0.0.1', 'admin888', '爱看体育', 'www.hfzeb.com', '体育', '增加', 'www.mnw.cn', 'http://www.html5code.net', 'http://sports.sohu.com/runner_b/index.shtml', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('880', '127.0.0.1', 'admin888', '虎啸军事', 'http://www.it-ht.com/', '军事', '增加', 'http://mil.qianlong.com/', '响应式', 'http://mil.qianlong.com/junshiyanjiu/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('881', '127.0.0.1', 'admin888', '历史资讯网', 'www.ajjie.com', '历史', '增加', 'http://www.lishitu.com/index.html', 'http://m.360changshi.com/', 'http://www.qulishi.com/jiemi/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('882', '127.0.0.1', 'admin888', '指尖艺术', 'www.ai5573.com', '艺术，名人', '增加', 'www.banhuajia.net', 'http://m.wj001.com/', 'http://www.iyipin.cn/nlist.php?cid=15', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('883', '127.0.0.1', 'admin888', '宝妈网', 'www.hj976.com', '孕妇 宝宝', '增加', 'http://www.163nvren.com/', 'http://m.39.net', 'http://www.baobmm.com/bm/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('884', '127.0.0.1', 'admin888', '酒状元', 'www.qiaoshitaiji.com', '酒类', '增加', 'www.jianiang.cn', 'http://www.html5code.net/', 'http://www.cien.com.cn/jiuye/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('885', '127.0.0.1', 'admin888', '魅力娱乐', 'www.pinghuxindai.org', '娱乐，新闻', '增加', 'http://www.rrlady.com/', '响应式', 'http://zgly.xinhuanet.com/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('886', '127.0.0.1', 'admin888', '美容资讯网', 'www.imdong.org', '美容', '增加', 'https://www.nvren.com/', '响应式（PC端改）', 'https://www.xineee.com/beauty/hairstyle/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('887', '127.0.0.1', 'admin888', '爱味客', 'www.tek-life.org', '美食', '增加', 'http://www.sznews.com/eating/index.htm', 'http://wap.qingdaonews.com/news.php', 'http://www.sznews.com/eating/index.htm', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('888', '127.0.0.1', 'admin888', '水创环保', 'www.reicmodel.org', '环保', '增加', 'http://www.hbzhan.com/news/', 'http://m.zixuntop.com', 'http://www.cfej.net/news/jjzwrtq/', '2019-09-07 09:30:03');
INSERT INTO `history` VALUES ('889', '127.0.0.1', 'admin888', '华美互联网', 'www.aidefence.org', '互联网', '增加', 'https://www.bh5.com/', '响应式（PC端改）', 'http://www.mnw.cn/keji/internet/', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('890', '127.0.0.1', 'admin888', '123情感资讯网', 'www.ngceu.cn', '情感', '增加', 'www.wulinjj.com', '响应式（PC端改）', 'http://qinggan.163nvren.com/hunyin/', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('891', '127.0.0.1', 'admin888', '茶之道', 'www.kutom.cn', '茶文化', '增加', 'http://www.puercn.com/', 'http://m.puercn.com/', 'http://cyw.t0001.com/news/list.php?catid=9', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('892', '127.0.0.1', 'admin888', '爱心公益', 'www.cqay.org', '公益新闻', '增加', 'http://www.wxxyx.cn/', 'https://www.cndsw.com.cn/m/', 'http://picture.yunnan.cn/', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('893', '127.0.0.1', 'admin888', '正信房产', 'www.465100.net', '房产新闻', '增加', 'www.xwkx.net', 'http://m.mnw.cn', 'http://news.xafc.com/list-343-1.html', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('894', '127.0.0.1', 'admin888', '乐活时尚网', 'www.lipuchao.cn', '时尚', '增加', 'http://www.lelady.cn/', '响应式（PC端改）', 'http://www.seoou.com/a/fushi/', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('895', '127.0.0.1', 'admin888', '鑫鑫旅游网', 'www.tianxiawusong.com', '旅游', '增加', 'www.trends.com.cn', '响应式（PC端改）', 'http://www.71lady.net/lvyou/chuyouluxian/guoneijingdian/', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('896', '127.0.0.1', 'admin888', '大众足球篮球资讯', 'http://www.aluregirls.org/', '篮球，足球', '增加', 'https://www.haonanren.cm/', '响应式', 'https://www.iuliao.com/news/category/3', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('897', '127.0.0.1', 'admin888', '全民健身', 'www.zlfsyy.org', '健身', '增加', 'www.muscles.com.cn', 'https://m.muscles.com.cn', 'https://www.jirou.com/jirou/cx', '2019-09-07 09:30:04');
INSERT INTO `history` VALUES ('898', '127.0.0.1', 'admin888', '薄荷生活网', 'www.linduduo.org', '生活', '增加', 'www.zizhongjianshen.com', '响应式（PC端改）', 'https://www.eastlady.cn/life/mbxqm/', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('899', '127.0.0.1', 'admin888', '潮流手机网', 'www.hrlzsj.org', '手机', '增加', 'http://www.onlylady.com/', '响应式（PC端改）', 'http://www.cnmo.com/news/message/', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('900', '127.0.0.1', 'admin888', '珠光宝气', 'www.1024xp.org', '珠宝', '增加', 'https://www.94hnr.com/', '响应式（PC端改）', 'http://www.chinajeweler.com/zbxf/jiehun/', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('901', '127.0.0.1', 'admin888', '摄影的艺术', 'www.lab205.org', '摄影', '增加', 'http://www.7y7.com/', '响应式（PC端改）', 'http://www.szseven.com/ProductsStd_235.html', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('902', '127.0.0.1', 'admin888', '时尚家居', 'www.shiwawang.cn', '家居', '增加', 'http://www.trendsgroup.com.cn/', '响应式（PC端改）', 'http://www.ellechina.com/deco/gallery/space/', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('903', '127.0.0.1', 'admin888', '电影娱乐网', 'www.dz-ctsm.cn', '电影', '增加', 'http://www.mingxing.com/', '响应式（PC端改）', 'http://www.dianyingjie.com/world/', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('904', '127.0.0.1', 'admin888', '尚中医', 'www.androider.org', '中医学', '增加', 'http://zy.chzy.org.cn', 'http://m.99.com.cn', 'http://www.tcmregimen.com/?cat=9', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('905', '127.0.0.1', 'admin888', 'IQ男人', 'www.17san.cn', '男人，时尚', '增加', 'http://www.haonanren.cm', 'http://wap.haonanren.cm', 'https://www.nanrenwo.net/nrjcz/fuzhuangdapei/', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('906', '127.0.0.1', 'admin888', '爱看明星网', 'www.51zzly.cn', '明星', '增加', 'http://www.mavees-shop.com/', '响应式（PC端改）', 'http://www.mingxing.com/news/index/type/dyxw.html', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('907', '127.0.0.1', 'admin888', '奇妙网', 'www.hzxmz.cn', '', '增加', '', '', '', '2019-09-07 09:30:05');
INSERT INTO `history` VALUES ('908', '127.0.0.1', 'admin888', '佳成美发', 'www.biomaker.cc', '美发', '增加', 'http://www.faxingnet.com', '响应式（PC端改）', 'http://www.kxxh.net', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('909', '127.0.0.1', 'admin888', '纤尚美', 'www.zibr.cc', '减肥', '增加', 'http://www.laonanren.com', 'http://m.laonanren.com', 'http://www.jianfeiing.com/html/jianfeiqiaomen', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('910', '127.0.0.1', 'admin888', '尚雅美妆', 'www.52qth.cn', '美妆', '增加', 'http://www.stylemode.com', 'http://m.stylemode.com', 'http://www.38fan.com/caizhuanggongju', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('911', '127.0.0.1', 'admin888', '99奢华', 'www.hellosoul.net', '奢华品', '增加', 'http://www.hao99.com', '响应式（PC端改）', 'https://www.nanrenwo.net/watch/collection', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('912', '127.0.0.1', 'admin888', '爱尚婚姻', 'www.isskss.com', '婚姻', '增加', 'www.menww.com', '响应式（PC端改）', 'http://www.bashalady.com/hunjia', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('913', '127.0.0.1', 'admin888', '看星座', 'www.gyjpw.com', '星座', '增加', 'http://www.aorenod.com', '响应式', 'http://www.meiguoshenpo.com/fengshui/', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('914', '127.0.0.1', 'admin888', '陶技网', 'www.51dmk.com', '', '增加', '', '', '', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('915', '127.0.0.1', 'admin888', '快乐亲子', 'www.hrbdiet.com', '亲子', '增加', 'http://www.ishaohuang.com/', '响应式（PC端改）', 'https://www.38fan.com/qinzi', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('916', '127.0.0.1', 'admin888', '维度娱乐', 'www.shlgcg.com', '明星，娱乐', '增加', 'http://www.aorenod.com', '响应式', 'http://https://www.38fan.com/dianshijujuqing', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('917', '127.0.0.1', 'admin888', '世界之最网', 'www.gyjpw.com', '世界之最', '增加', 'http://www.weimeicun.com', 'http://m.weimeicun.com', 'http://www.zswxy.cn/anima', '2019-09-07 09:30:06');
INSERT INTO `history` VALUES ('918', '127.0.0.1', 'admin888', '66品牌网', 'www.yjtemple.org', '品牌', '增加', 'http://www.zhonggaoyi.cn', '响应式（PC端改）', 'http://www.zswxy.cn/brand', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('919', '127.0.0.1', 'admin888', '美文网', 'www.yz58.org', '文学，美文', '增加', 'http://www.docterpet.com', '响应式（PC端改）', 'https://www.meiwenmeiju.cn/juzi/gaoxiao/', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('920', '127.0.0.1', 'admin888', '柚子经典', 'www.brcfair.org', '搞笑段子笑话', '增加', 'http://www.0755zb.com', '响应式（PC端改）', 'https://www.818rmb.com/lzmy/', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('921', '127.0.0.1', 'admin888', '畅游游戏网', 'www.kctsxz.org', '游戏', '增加', '', '', '', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('922', '127.0.0.1', 'admin888', '吉祥急救网', 'www.lzsfas.org', '安全急救', '增加', 'http://sc.china.com.cn', '响应式（PC端改）', 'http://120.qm120.com', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('923', '127.0.0.1', 'admin888', '萌宠时代', 'www.wootv.org', '宠物医院', '增加', 'http://www.izhufu.net', '响应式（PC端改）', 'http://www.docterpet.com/cwyy/', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('924', '127.0.0.1', 'admin888', '风度汽车网', 'www.ysscdc.org', '汽车', '增加', '', '响应式（PC端改）', 'http://www.cvnews.com.cn/portal.php?mod=list&catid=27', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('925', '127.0.0.1', 'admin888', '满面春风', 'www.mop360.net', '段子，笑话', '增加', 'https://www.ixinwei.com', '响应式（PC端改）', 'http://www.taohuaan.net/s/1.html', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('926', '127.0.0.1', 'admin888', '花飞榭', 'www.teatickes.net', '花', '增加', '', '', '', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('927', '127.0.0.1', 'admin888', '智慧科技', 'www.dacszp.com', '科技生活', '增加', '', '', 'https://www.iyiou.com/Rail_Transit/', '2019-09-07 09:30:07');
INSERT INTO `history` VALUES ('928', '127.0.0.1', 'admin888', '全球电影新闻网', 'www.dnmyzs.com', '电影资讯', '增加', 'www.4hw.com.cn', '响应式（PC端改）', 'http://www.glofilm.com/film/huayu/', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('929', '127.0.0.1', 'admin888', '净佛教', 'www.e-shyn.com', '佛学', '增加', 'https://www.infogame.cn', '响应式（PC端改）', 'http://www.gming.org/fxrm/fhjt/', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('930', '127.0.0.1', 'admin888', '安心电商', 'www.huabeils.com', '电商', '增加', 'https://yule.youbian.com', 'https://m.youbian.com', 'http://www.dsb.cn', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('931', '127.0.0.1', 'admin888', '方拓生物', 'www.sangebang.net', '生物技术', '增加', 'https://www.jkthin.com', '响应式（PC端改）', 'http://www.bio1000.com', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('932', '127.0.0.1', 'admin888', '宇宙网', 'www.hzxmz.cn', '宇宙资讯', '增加', 'https://www.gq.com.cn', '响应式（PC端改）', 'https://space.qq.com/twtx.htm', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('933', '127.0.0.1', 'admin888', '顺康健康网', 'www.yxtzkg.com', '健康', '增加', 'https://yangsheng.120ask.com', '响应式（PC端改）', 'https://www.jk3721.com/health', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('934', '127.0.0.1', 'admin888', '幸福亲子', 'www.zzspread.com', '亲子资讯', '增加', 'www.yoyojiu.com', '响应式（PC端改）', 'https://www.mama.cn', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('935', '127.0.0.1', 'admin888', '享受生活', 'www.jxhtjn.com', '生活', '增加', 'www.jiachong.net', '响应式（PC端改）', 'https://www.mshishang.com', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('936', '127.0.0.1', 'admin888', '风度汽车网', 'www.ysscdc.org', '汽车', '增加', 'www.menworld.org', '响应式（PC端改）', 'http://www.cvnews.com.cn', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('937', '127.0.0.1', 'admin888', '恋爱技巧网', 'www.ilistening.net', '恋爱', '增加', 'www.77lux.com', '响应式（PC端改）', 'https://www.puamap.com', '2019-09-07 09:30:08');
INSERT INTO `history` VALUES ('938', '127.0.0.1', 'admin888', '科技探索', 'www.xiyangyang2.com', '探索科技', '增加', 'www.jp14.com', '响应式（PC端改）', 'http://www.tanmizhi.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('939', '127.0.0.1', 'admin888', '天星风水', 'www.honglangdoors.com', '看风水', '增加', 'www.suanming.com.cn', '响应式（PC端改）', 'http://www.xinfanwan.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('940', '127.0.0.1', 'admin888', '四季植物网', 'www.eeestyles.com', '植物', '增加', 'http://www.chinacaipu.com', '响应式（PC端改）', 'https://www.zw3e.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('941', '127.0.0.1', 'admin888', '美硕酒业', 'www.taidiyingli.com', '红酒资讯', '增加', 'http://www.zyzydq.com', '响应式（PC端改）', 'https://www.putaojiu.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('942', '127.0.0.1', 'admin888', '千帆漫画', 'www.chengbian.net', '漫画', '增加', 'http://www.meiyixia.com', 'http://m.meiyixia.com', 'http://www.pmjun.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('943', '127.0.0.1', 'admin888', '音乐百科', 'www.chen-xi.net', '音乐看点', '增加', 'http://www.zswxy.cn', '响应式（PC端改）', 'http://www.mnw.cn', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('944', '127.0.0.1', 'admin888', '百川教育', 'www.zly168.com', '教育', '增加', 'www.lieqidao.com', '响应式（PC端改）', 'http://www.rshgt.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('945', '127.0.0.1', 'admin888', '学佛故事', 'www.zitie.org', '佛家故事', '增加', 'http://www.infogame.cn', '响应式（PC端改）', 'http://www.gming.org', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('946', '127.0.0.1', 'admin888', '品尚农业', 'www.zhanyan.org', '农业资讯', '增加', '不详', '响应式（PC端改）', 'http://www.nyzy.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('947', '127.0.0.1', 'admin888', '建筑土木网', 'www.biyefun.com', '土木资讯', '增加', 'https://www.souid.com', '响应式（PC端改）', 'https://www.xianjichina.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('948', '127.0.0.1', 'admin888', '财经帮', 'www.liuzz.org', '理财', '增加', 'https://www.euroa.net', '响应式', 'http://fintech.xinhua08.com', '2019-09-07 09:30:09');
INSERT INTO `history` VALUES ('949', '127.0.0.1', 'admin888', '电力资讯网', 'www.hnexue.org', '电力资讯', '增加', 'https://www.qianzhan.com', 'https://m.qianzhan.com', 'https://www.xianjichina.com/news', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('950', '127.0.0.1', 'admin888', '玩游戏', 'www.wadh.org', '游戏资讯', '增加', 'https://www.dangdaigongyi.com.cn', 'https://m.dangdaigongyi.com.cn', 'https://http://www.mnw.cn', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('951', '127.0.0.1', 'admin888', '民俗网', 'www.techface.org', '风俗，民俗', '增加', 'http://www.dnzs678.com/', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('952', '127.0.0.1', 'admin888', '九宏家电', 'www.shenghai.org', '电器', '增加', '不详', '响应式（PC端改）', 'http://www.tidejd.com', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('953', '127.0.0.1', 'admin888', '汽车资讯网', 'www.sinoicity.cn', '汽车', '增加', 'http://news.huahuo.com', 'http://m.huahuo.com', 'http://www.chebiaow.com', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('954', '127.0.0.1', 'admin888', '唯情网', 'www.aichaojj.cn', '情感文章', '增加', '', 'http://www.manshijian.com', 'http://m.manshijian.com', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('955', '127.0.0.1', 'admin888', '论历史', 'www.hy-elec.cn', '历史资讯', '增加', 'http://www.nanchongzol.com', 'http://m.nanchongzol.com', 'http://www.360changshi.com', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('956', '127.0.0.1', 'admin888', 'M职场网', 'www.sdicdt.cn', '职场', '增加', 'http://www.faxingzhan.com', '响应式（PC端改）', 'http://www.360changshi.com', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('957', '127.0.0.1', 'admin888', '电脑常识网', 'www.07ms.cn', '电脑常识', '增加', '不详', '响应式（PC端改）', 'http://www.dnzs678.com', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('958', '127.0.0.1', 'admin888', '智慧电脑网', 'www.bbwqc.cn', '电脑', '增加', '不详', '响应式（PC端改）', 'http://www.dngsw.cn', '2019-09-07 09:30:10');
INSERT INTO `history` VALUES ('959', '127.0.0.1', 'admin888', '时尚签名网', 'www.meixim.cn', '签名，头像', '增加', 'https://www.duwzw.com', '响应式（PC端改）', 'https://www.feizl.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('960', '127.0.0.1', 'admin888', '暗夜手机网', 'www.pcuser.cn', '手机', '增加', 'http://www.jinlila.com', '响应式（PC端改）', 'http://www.cnmo.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('961', '127.0.0.1', 'admin888', '纤姿雅', 'www.ckdmro.com', '减肥，肥胖', '增加', 'http://www.520730.com', 'http://m.520730.com', 'http://www.jianfei.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('962', '127.0.0.1', 'admin888', '婚嫁攻略', 'www.gladsin.cn', '婚礼，结婚', '增加', 'http://www.dianping.com', 'http://m.dianping.com', 'https://www.hunmanyi.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('963', '127.0.0.1', 'admin888', '时尚秀', 'www.questex.cn', '服装', '增加', 'http://www.mimito.com.cn', '响应式', 'http://www.hao528.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('964', '127.0.0.1', 'admin888', '遇见情感网', 'www.ziti365.com', '情感资讯', '增加', 'http://www.zgsspw.com', 'http://m.zgsspw.com', 'http://www.hao528.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('965', '127.0.0.1', 'admin888', '幸福窝', 'www.xinxinsolar.com', '家居', '增加', 'http://www.juziss.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('966', '127.0.0.1', 'admin888', '装修资讯网', 'www.flconsulte.com', '装修资讯', '增加', 'http://www.qiguaiw.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('967', '127.0.0.1', 'admin888', '装修施工网', 'www.tysysm.com', '装修施工', '增加', 'http://www.nuuha.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('968', '127.0.0.1', 'admin888', '优质生活', 'www.bajieol.com', '生活', '增加', 'http://www.hunmanyi.com', 'http://m.hunmanyi.com', 'https://www.38fan.com', '2019-09-07 09:30:11');
INSERT INTO `history` VALUES ('969', '127.0.0.1', 'admin888', '看好房', 'www.dxg89.com', '看房', '增加', 'http://www.xieshudeng.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('970', '127.0.0.1', 'admin888', '买好房', 'www.q6u.com', '买房', '增加', 'http://www.q6u.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('971', '127.0.0.1', 'admin888', '最资讯', 'www.mfcad.com', '世界之最', '增加', 'www.mfcad.com', '响应式（PC端改）', 'https://www.souid.com/', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('972', '127.0.0.1', 'admin888', '快科普', 'www.mfcad.com', '科普', '增加', '不详', '响应式（PC端改）', 'https://www.souid.com/', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('973', '127.0.0.1', 'admin888', '潮鞋帮', 'www.fujitech-sh.com', '鞋子', '增加', 'http://www.jiushixing.com', 'http://m.jiushixing.com', 'http://www.q6u.com', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('974', '127.0.0.1', 'admin888', '亲子宝贝', 'www.cszlg.com', '亲子', '增加', 'http://www.hao528.com', 'http://m.hao528.com', 'http://www.5h.com', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('975', '127.0.0.1', 'admin888', '糖果娱乐', 'www.hbxingjian.com', '娱乐，明星', '增加', 'http://www.lamaying.com', 'http://m.lamaying.com', 'https://www.ixiumei.com', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('976', '127.0.0.1', 'admin888', 'kiki私宠', 'www.atwoodmats.com', '宠物猫', '增加', 'http://qmjs.tiqiu.com', 'http://m.qmjs.tiqiu.com', 'http://www.boqii.com', '2019-09-07 09:30:12');
INSERT INTO `history` VALUES ('977', '127.0.0.1', 'admin888', '水族之家', 'www.wiyaphotos.com', '水族', '增加', 'http://www.cosmofashion.com.cn', 'http://m.cosmofashion.com.cn', 'http://www.boqii.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('978', '127.0.0.1', 'admin888', '绿缘植物', 'www.dualwarez.com', '植物', '增加', 'http://www.nfyl.net', 'http://m.nfyl.net', 'http://www.boqii.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('979', '127.0.0.1', 'admin888', '普生医疗', '', '医疗', '增加', 'http://www.tz.ef360.com', 'http://m.tz.ef360.com', 'https://www.xianjichina.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('980', '127.0.0.1', 'admin888', '爱尚摄影', '', '摄影', '增加', 'http://www.shunvzhi.com', 'http://m.shunvzhi.com', 'http://www.nitutu.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('981', '127.0.0.1', 'admin888', '时尚数码', '', '数码', '增加', 'http://www.handanol.com', 'http://m.handanol.com', 'https://www.nanrenwo.net', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('982', '127.0.0.1', 'admin888', '爱美容', '', '美容', '增加', 'http://www.faxingsj.com', 'http://m.faxingsj.com', 'http://www.meifajie.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('983', '127.0.0.1', 'admin888', '东方影视', '', '影视', '增加', 'http://http://www.365j.com', 'http://m.365j.com', 'http://news.ylq.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('984', '127.0.0.1', 'admin888', '西方影视', '', '影视', '增加', 'http://www.ylq.com', 'http://m.ylq.com', 'http://yule.youbian.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('985', '127.0.0.1', 'admin888', '香吉士', 'www.jpsbzr.com', '香水', '增加', 'http://www.meiyanw.com', 'https://mip.nvren.com', 'https://www.nvren.com', '2019-09-07 09:30:13');
INSERT INTO `history` VALUES ('986', '127.0.0.1', 'admin888', '丽人彩妆', '', '彩妆攻略', '增加', 'http://www.meirijinrong.com', 'http://m.meirijinrong.com', 'https://www.nvren.com', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('987', '127.0.0.1', 'admin888', '悠悠美发', 'www.559464.com', '美发', '增加', 'http://www.movie.itedou.com', 'http://m.movie.itedou.com', 'https://www.eastlady.cn', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('988', '127.0.0.1', 'admin888', '潮流搭配', '', '时尚搭配', '增加', 'http://www.women-health.cn', 'http://m.women-health.cn', 'https://www.eastlady.cn', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('989', '127.0.0.1', 'admin888', '爱狗网', 'http://www.ichong123.com/', '宠物，狗', '增加', 'http://www.ichong123.com/', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('990', '127.0.0.1', 'admin888', '小宠网', 'http://www.ichong123.com/', '小型宠物', '增加', 'www.bianzhirensheng.com', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('991', '127.0.0.1', 'admin888', '爬行宠物网', 'http://www.ichong123.com/', '小型宠物 爬虫宠物 爬行宠物', '增加', '不详', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('992', '127.0.0.1', 'admin888', '小丑熊', 'http://www.ichong123.com/', '亲子', '增加', '不详', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('993', '127.0.0.1', 'admin888', '健康生活', 'http://www.ichong123.com/', '生活', '增加', 'dc.pchome.net', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('994', '127.0.0.1', 'admin888', '女性健康', 'http://www.ichong123.com/', '妇科 保健 健康', '增加', 'www.intfad.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:14');
INSERT INTO `history` VALUES ('995', '127.0.0.1', 'admin888', '新派饮食', 'http://www.ichong123.com/', '美食', '增加', 'www.achongwu.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('996', '127.0.0.1', 'admin888', '电子信息', 'http://www.ichong123.com/', '电子 信息技术', '增加', 'www.qinbaol.com', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('997', '127.0.0.1', 'admin888', '机械工程', 'http://www.ichong123.com/', '机械 工程', '增加', 'http://news.szhk.com/', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('998', '127.0.0.1', 'admin888', '化学化工', 'http://www.ichong123.com/', '化学 工程', '增加', 'ent.qianzhan.com', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('999', '127.0.0.1', 'admin888', '媒体报道', 'http://www.ichong123.com/', '媒体 报道 资讯', '增加', 'http://yl.szhk.com/', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('1000', '127.0.0.1', 'admin888', '人群健康', 'http://www.ichong123.com/', '人群 健康', '增加', 'www.meifajie.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('1001', '127.0.0.1', 'admin888', '今日百科', 'http://www.ichong123.com/', '百科 生活', '增加', 'www.hanjutv.com', '响应式（PC端改）', 'https://www.eastlady.cn/baike/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('1002', '127.0.0.1', 'admin888', '我爱文章网', 'http://www.ichong123.com/', '文章 好词好句 经典', '增加', 'www.pcoof.com', 'http://mip.duanmeiwen.com/lizhi/', 'http://www.jingdianwenzhang.cn/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('1003', '127.0.0.1', 'admin888', '悠悠减肥', 'http://www.ichong123.com/', '减肥 生活', '增加', 'www.aitaotu.com', '响应式（PC端改）', 'https://www.eastlady.cn/baike/', '2019-09-07 09:30:15');
INSERT INTO `history` VALUES ('1004', '127.0.0.1', 'admin888', '悠悠减肥', '未知', '减肥 生活', '修改', 'www.aitaotu.com', '响应式（PC端改）', 'https://www.eastlady.cn/baike/', '2019-09-07 18:05:25');
INSERT INTO `history` VALUES ('1005', '127.0.0.1', 'admin888', '我爱文章网', '未知', '文章 好词好句 经典', '修改', 'www.pcoof.com', 'http://mip.duanmeiwen.com/lizhi/', 'http://www.jingdianwenzhang.cn/', '2019-09-07 18:05:37');
INSERT INTO `history` VALUES ('1006', '127.0.0.1', 'admin888', '今日百科', '未知', '百科 生活', '修改', 'www.hanjutv.com', '响应式（PC端改）', 'https://www.eastlady.cn/baike/', '2019-09-07 18:05:41');
INSERT INTO `history` VALUES ('1007', '127.0.0.1', 'admin888', '人群健康', '未知', '人群 健康', '修改', 'www.meifajie.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 18:05:49');
INSERT INTO `history` VALUES ('1008', '127.0.0.1', 'admin888', '媒体报道', '未知', '媒体 报道 资讯', '修改', 'http://yl.szhk.com/', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 18:05:53');
INSERT INTO `history` VALUES ('1009', '127.0.0.1', 'admin888', '化学化工', '未知', '化学 工程', '修改', 'ent.qianzhan.com', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 18:05:58');
INSERT INTO `history` VALUES ('1010', '127.0.0.1', 'admin888', '机械工程', '未知', '机械 工程', '修改', 'http://news.szhk.com/', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 18:06:03');
INSERT INTO `history` VALUES ('1011', '127.0.0.1', 'admin888', '电子信息', '未知', '电子 信息技术', '修改', 'www.qinbaol.com', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 18:06:08');
INSERT INTO `history` VALUES ('1012', '127.0.0.1', 'admin888', '新派饮食', '未知', '美食', '修改', 'www.achongwu.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 18:06:13');
INSERT INTO `history` VALUES ('1013', '127.0.0.1', 'admin888', '女性健康', '未知', '妇科 保健 健康', '修改', 'www.intfad.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 18:06:17');
INSERT INTO `history` VALUES ('1014', '127.0.0.1', 'admin888', '健康生活', '未知', '生活', '修改', 'dc.pchome.net', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 18:06:50');
INSERT INTO `history` VALUES ('1015', '127.0.0.1', 'admin888', '小丑熊', '未知', '亲子', '修改', '不详', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 18:07:00');
INSERT INTO `history` VALUES ('1016', '127.0.0.1', 'admin888', '爬行宠物网', '未知', '小型宠物 爬虫宠物 爬行宠物', '修改', '不详', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 18:07:07');
INSERT INTO `history` VALUES ('1017', '127.0.0.1', 'admin888', '小宠网', '未知', '小型宠物', '修改', 'www.bianzhirensheng.com', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 18:07:13');
INSERT INTO `history` VALUES ('1018', '127.0.0.1', 'admin888', '爱狗网', '未知', '宠物，狗', '修改', 'http://www.ichong123.com/', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 18:07:19');

-- ----------------------------
-- Table structure for msg
-- ----------------------------
DROP TABLE IF EXISTS `msg`;
CREATE TABLE `msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(10) NOT NULL,
  `msg` varchar(100) NOT NULL,
  `time` datetime NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of msg
-- ----------------------------
INSERT INTO `msg` VALUES ('7', 'admin888', 'hello world', '2022-01-06 09:34:10', '../static/img/avatar-5d723a07e38b6.jpg');
INSERT INTO `msg` VALUES ('8', 'admin888', '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '2022-01-06 09:38:34', '../static/img/avatar-5d723a07e38b6.jpg');

-- ----------------------------
-- Table structure for template
-- ----------------------------
DROP TABLE IF EXISTS `template`;
CREATE TABLE `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `title` varchar(100) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `pc_url` varchar(100) DEFAULT NULL,
  `mobile_url` varchar(100) DEFAULT NULL,
  `content_url` varchar(100) DEFAULT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=970 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of template
-- ----------------------------
INSERT INTO `template` VALUES ('9', '杨鑫', '东方新闻网', 'www.hiphopcc.com', null, null, null, null, '2019-05-17 09:09:14');
INSERT INTO `template` VALUES ('10', '杨鑫', '看奇闻', 'www.cyxlyz.com', null, null, null, null, '2019-05-17 09:10:30');
INSERT INTO `template` VALUES ('11', '杨鑫', '云梦比特币新闻网', 'www.gaojige.com', null, null, null, null, '2019-05-17 09:11:27');
INSERT INTO `template` VALUES ('12', '杨鑫', '孝感娱乐资讯网', 'www.ihuolong.com', null, null, null, null, '2019-05-17 09:12:13');
INSERT INTO `template` VALUES ('13', '杨鑫', '蔡甸律师事务所', 'www.china-awesome.com', null, null, null, null, '2019-05-17 09:13:09');
INSERT INTO `template` VALUES ('14', '杨鑫', '硚口文章网', 'www.0471dy.com', null, null, null, null, '2019-05-17 09:13:40');
INSERT INTO `template` VALUES ('15', '杨鑫', '新县文章网', 'www.jlsttyy.com', null, null, null, null, '2019-05-17 09:14:40');
INSERT INTO `template` VALUES ('16', '杨鑫', '洪山新闻网', 'www.jhljzz.com', null, null, null, null, '2019-05-17 09:15:45');
INSERT INTO `template` VALUES ('17', '杨鑫', '光谷创业IT资讯网', 'www.17ixue.com', null, null, null, null, '2019-05-17 09:16:40');
INSERT INTO `template` VALUES ('18', '杨鑫', '关山手机资讯网', 'www.repoequipmentsales.com', null, null, null, null, '2019-05-17 09:19:45');
INSERT INTO `template` VALUES ('19', '杨鑫', '黄陂文章网', 'www.casaperferiebetania.com', null, null, null, null, '2019-05-17 09:20:41');
INSERT INTO `template` VALUES ('20', '杨鑫', '阳逻文章网', 'www.robustfastlock.com', null, null, null, null, '2019-05-17 09:23:30');
INSERT INTO `template` VALUES ('21', '杨鑫', '江岸新闻网', 'www.katellachurch.com', null, null, null, null, '2019-05-17 09:24:17');
INSERT INTO `template` VALUES ('22', '杨鑫', '东西湖新闻网', 'www.wirelesscompanys.com', null, null, null, null, '2019-05-17 09:26:19');
INSERT INTO `template` VALUES ('23', '杨鑫', '汉口新闻网', 'www.aaroneye.com', null, null, null, null, '2019-05-17 09:27:07');
INSERT INTO `template` VALUES ('24', 'admin', '古田个性签名网', 'www.xzhgw.com', null, '', null, '', '2019-05-17 09:27:50');
INSERT INTO `template` VALUES ('25', '杨鑫', '东湖科技资讯', 'www.videoclipsdump.com', null, null, null, null, '2019-05-17 09:28:28');
INSERT INTO `template` VALUES ('26', '杨鑫', '当阳娱乐网', 'www.sf807.com', null, null, null, null, '2019-06-03 09:54:13');
INSERT INTO `template` VALUES ('27', '杨鑫', '龙岗汽车网', 'www.sasgeobat.com', null, null, null, null, '2019-06-03 09:55:57');
INSERT INTO `template` VALUES ('28', '杨鑫', '蛇口教育资讯网', 'http://www.f-vs.net', null, null, null, null, '2019-06-03 09:57:41');
INSERT INTO `template` VALUES ('29', '杨鑫', '团风新闻网', 'http://www.acxf.net/', null, null, null, null, '2019-06-03 10:00:23');
INSERT INTO `template` VALUES ('30', '杨鑫', '衡水时尚网', 'www.gdcml.com', null, null, null, null, '2019-06-10 14:18:45');
INSERT INTO `template` VALUES ('31', '杨鑫', '常德图片网', 'http://www.zanzicrea.com/', null, null, null, null, '2019-06-10 14:19:12');
INSERT INTO `template` VALUES ('32', '杨鑫', '新乡动漫资讯网', 'http://www.rohm-ic.com/', null, null, null, null, '2019-06-10 14:19:38');
INSERT INTO `template` VALUES ('33', '杨鑫', '红安物联网', 'http://www.jingsefushi.com/', null, null, null, null, '2019-06-10 14:20:09');
INSERT INTO `template` VALUES ('34', '杨鑫', '通城房产资讯网', 'http://tiger-fy.com', null, '', null, '', '2019-06-10 14:21:07');
INSERT INTO `template` VALUES ('831', 'admin888', '第一预测网', 'www.ly-u.com', '星座、生肖、风水', 'https://www.d1xz.net', 'https://3g.d1xz.net', 'https://www.d1xz.net', '2019-09-07 09:30:01');
INSERT INTO `template` VALUES ('832', 'admin888', '萌宠生活馆', 'www.454950.net', '宠物', 'http://www.chongbaba.com/', 'https://m.ichong123.com/', 'http://www.sohu.com/', '2019-09-07 09:30:01');
INSERT INTO `template` VALUES ('833', 'admin888', '人人教育网', 'http://www.ie-ter.com', '教育', 'http://www.jyb.cn/', 'https://mip.5djiaren.com/', 'http://news.e21.cn/  http://news.haedu.cn', '2019-09-07 09:30:01');
INSERT INTO `template` VALUES ('834', 'admin888', '游知有戏', 'www.sjzfgl.com', '游戏', 'http://www.ali213.net/news/', 'http://3g.ali213.net/news/', 'http://www.doyo.cn', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('835', 'admin888', '衣搭时尚', 'www.rrydss.com', '衣服、护肤、娱乐', 'http://www.mimito.com.cn/', '响应式', 'http://www.7624.net/  http://www.mimito.com.cn', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('836', 'admin888', '素食天下', 'www.grtd.net', '养生、素食', 'http://www.ssysw.cn/', 'http://m.yuexw.com/', 'http://www.chinavegan.com', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('837', 'admin888', '爱车有道网', 'www.hlmusicworld.com', '汽车', 'http://www.chinaautonews.com.cn/', 'http://www.html5code.net/mobile/plugin-3395.html', 'https://www.autotimes.com.cn', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('838', 'admin888', '以食养身', 'www.ccjlgg.com', '美食、养生', 'http://www.ttys5.com/', 'http://www.html5code.net/mobile/plugin-3393.html', 'http://www.lzyysw.com', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('839', 'admin888', '天天保健', 'http://www.dotanb.com/', '保健，按摩', 'http://www.cayb.net/', '响应式', 'http://care.39.net/dzbj/baby/', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('840', 'admin888', '二次元网', 'www.soyhw.com', '动漫', 'http://www.005.tv/', '响应式', 'http://acg.gamersky.com/pic/cosplay/', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('841', 'admin888', '友鼎金融网', 'www.dy-tm.com', '金融', 'http://www.ahjr.com.cn/', '响应式', 'http://finance.fjndwb.com/', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('842', 'admin888', '荣星数码', 'www.ituye.com', '数码产品', 'http://www.hsw.cn/', 'http://m.dtphoto.com/sj/2018/12/11/qfdmopap.html', 'http://digi.hsw.cn/smjd/', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('843', 'admin888', '兴民农业资讯', 'www.zjkyqb.com', '农业', 'http://www.sjwz.cn/', 'http://meirongbjw.yilianapp.com/', 'care.39.net/ys/shxg/', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('844', 'admin888', '潮， 范儿', 'www.1982y.cn', '潮流，服装，鞋', 'http://www.azs.org.cn/', 'http://m.china-ef.com/', 'http://www.cnxz.cn/news/category-10-1.html', '2019-09-07 09:30:02');
INSERT INTO `template` VALUES ('845', 'admin888', '爱看体育', 'www.hfzeb.com', '体育', 'www.mnw.cn', 'http://www.html5code.net', 'http://sports.sohu.com/runner_b/index.shtml', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('846', 'admin888', '虎啸军事', 'http://www.it-ht.com/', '军事', 'http://mil.qianlong.com/', '响应式', 'http://mil.qianlong.com/junshiyanjiu/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('847', 'admin888', '历史资讯网', 'www.ajjie.com', '历史', 'http://www.lishitu.com/index.html', 'http://m.360changshi.com/', 'http://www.qulishi.com/jiemi/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('848', 'admin888', '指尖艺术', 'www.ai5573.com', '艺术，名人', 'www.banhuajia.net', 'http://m.wj001.com/', 'http://www.iyipin.cn/nlist.php?cid=15', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('849', 'admin888', '宝妈网', 'www.hj976.com', '孕妇 宝宝', 'http://www.163nvren.com/', 'http://m.39.net', 'http://www.baobmm.com/bm/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('850', 'admin888', '酒状元', 'www.qiaoshitaiji.com', '酒类', 'www.jianiang.cn', 'http://www.html5code.net/', 'http://www.cien.com.cn/jiuye/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('851', 'admin888', '魅力娱乐', 'www.pinghuxindai.org', '娱乐，新闻', 'http://www.rrlady.com/', '响应式', 'http://zgly.xinhuanet.com/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('852', 'admin888', '美容资讯网', 'www.imdong.org', '美容', 'https://www.nvren.com/', '响应式（PC端改）', 'https://www.xineee.com/beauty/hairstyle/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('853', 'admin888', '爱味客', 'www.tek-life.org', '美食', 'http://www.sznews.com/eating/index.htm', 'http://wap.qingdaonews.com/news.php', 'http://www.sznews.com/eating/index.htm', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('854', 'admin888', '水创环保', 'www.reicmodel.org', '环保', 'http://www.hbzhan.com/news/', 'http://m.zixuntop.com', 'http://www.cfej.net/news/jjzwrtq/', '2019-09-07 09:30:03');
INSERT INTO `template` VALUES ('855', 'admin888', '华美互联网', 'www.aidefence.org', '互联网', 'https://www.bh5.com/', '响应式（PC端改）', 'http://www.mnw.cn/keji/internet/', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('856', 'admin888', '123情感资讯网', 'www.ngceu.cn', '情感', 'www.wulinjj.com', '响应式（PC端改）', 'http://qinggan.163nvren.com/hunyin/', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('857', 'admin888', '茶之道', 'www.kutom.cn', '茶文化', 'http://www.puercn.com/', 'http://m.puercn.com/', 'http://cyw.t0001.com/news/list.php?catid=9', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('858', 'admin888', '爱心公益', 'www.cqay.org', '公益新闻', 'http://www.wxxyx.cn/', 'https://www.cndsw.com.cn/m/', 'http://picture.yunnan.cn/', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('859', 'admin888', '正信房产', 'www.465100.net', '房产新闻', 'www.xwkx.net', 'http://m.mnw.cn', 'http://news.xafc.com/list-343-1.html', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('860', 'admin888', '乐活时尚网', 'www.lipuchao.cn', '时尚', 'http://www.lelady.cn/', '响应式（PC端改）', 'http://www.seoou.com/a/fushi/', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('861', 'admin888', '鑫鑫旅游网', 'www.tianxiawusong.com', '旅游', 'www.trends.com.cn', '响应式（PC端改）', 'http://www.71lady.net/lvyou/chuyouluxian/guoneijingdian/', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('862', 'admin888', '大众足球篮球资讯', 'http://www.aluregirls.org/', '篮球，足球', 'https://www.haonanren.cm/', '响应式', 'https://www.iuliao.com/news/category/3', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('863', 'admin888', '全民健身', 'www.zlfsyy.org', '健身', 'www.muscles.com.cn', 'https://m.muscles.com.cn', 'https://www.jirou.com/jirou/cx', '2019-09-07 09:30:04');
INSERT INTO `template` VALUES ('864', 'admin888', '薄荷生活网', 'www.linduduo.org', '生活', 'www.zizhongjianshen.com', '响应式（PC端改）', 'https://www.eastlady.cn/life/mbxqm/', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('865', 'admin888', '潮流手机网', 'www.hrlzsj.org', '手机', 'http://www.onlylady.com/', '响应式（PC端改）', 'http://www.cnmo.com/news/message/', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('866', 'admin888', '珠光宝气', 'www.1024xp.org', '珠宝', 'https://www.94hnr.com/', '响应式（PC端改）', 'http://www.chinajeweler.com/zbxf/jiehun/', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('867', 'admin888', '摄影的艺术', 'www.lab205.org', '摄影', 'http://www.7y7.com/', '响应式（PC端改）', 'http://www.szseven.com/ProductsStd_235.html', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('868', 'admin888', '时尚家居', 'www.shiwawang.cn', '家居', 'http://www.trendsgroup.com.cn/', '响应式（PC端改）', 'http://www.ellechina.com/deco/gallery/space/', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('869', 'admin888', '电影娱乐网', 'www.dz-ctsm.cn', '电影', 'http://www.mingxing.com/', '响应式（PC端改）', 'http://www.dianyingjie.com/world/', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('870', 'admin888', '尚中医', 'www.androider.org', '中医学', 'http://zy.chzy.org.cn', 'http://m.99.com.cn', 'http://www.tcmregimen.com/?cat=9', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('871', 'admin888', 'IQ男人', 'www.17san.cn', '男人，时尚', 'http://www.haonanren.cm', 'http://wap.haonanren.cm', 'https://www.nanrenwo.net/nrjcz/fuzhuangdapei/', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('872', 'admin888', '爱看明星网', 'www.51zzly.cn', '明星', 'http://www.mavees-shop.com/', '响应式（PC端改）', 'http://www.mingxing.com/news/index/type/dyxw.html', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('873', 'admin888', '奇妙网', 'www.hzxmz.cn', '', '', '', '', '2019-09-07 09:30:05');
INSERT INTO `template` VALUES ('874', 'admin888', '佳成美发', 'www.biomaker.cc', '美发', 'http://www.faxingnet.com', '响应式（PC端改）', 'http://www.kxxh.net', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('875', 'admin888', '纤尚美', 'www.zibr.cc', '减肥', 'http://www.laonanren.com', 'http://m.laonanren.com', 'http://www.jianfeiing.com/html/jianfeiqiaomen', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('876', 'admin888', '尚雅美妆', 'www.52qth.cn', '美妆', 'http://www.stylemode.com', 'http://m.stylemode.com', 'http://www.38fan.com/caizhuanggongju', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('877', 'admin888', '99奢华', 'www.hellosoul.net', '奢华品', 'http://www.hao99.com', '响应式（PC端改）', 'https://www.nanrenwo.net/watch/collection', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('878', 'admin888', '爱尚婚姻', 'www.isskss.com', '婚姻', 'www.menww.com', '响应式（PC端改）', 'http://www.bashalady.com/hunjia', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('879', 'admin888', '看星座', 'www.gyjpw.com', '星座', 'http://www.aorenod.com', '响应式', 'http://www.meiguoshenpo.com/fengshui/', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('880', 'admin888', '陶技网', 'www.51dmk.com', '', '', '', '', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('881', 'admin888', '快乐亲子', 'www.hrbdiet.com', '亲子', 'http://www.ishaohuang.com/', '响应式（PC端改）', 'https://www.38fan.com/qinzi', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('882', 'admin888', '维度娱乐', 'www.shlgcg.com', '明星，娱乐', 'http://www.aorenod.com', '响应式', 'http://https://www.38fan.com/dianshijujuqing', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('883', 'admin888', '世界之最网', 'www.gyjpw.com', '世界之最', 'http://www.weimeicun.com', 'http://m.weimeicun.com', 'http://www.zswxy.cn/anima', '2019-09-07 09:30:06');
INSERT INTO `template` VALUES ('884', 'admin888', '66品牌网', 'www.yjtemple.org', '品牌', 'http://www.zhonggaoyi.cn', '响应式（PC端改）', 'http://www.zswxy.cn/brand', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('885', 'admin888', '美文网', 'www.yz58.org', '文学，美文', 'http://www.docterpet.com', '响应式（PC端改）', 'https://www.meiwenmeiju.cn/juzi/gaoxiao/', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('886', 'admin888', '柚子经典', 'www.brcfair.org', '搞笑段子笑话', 'http://www.0755zb.com', '响应式（PC端改）', 'https://www.818rmb.com/lzmy/', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('887', 'admin888', '畅游游戏网', 'www.kctsxz.org', '游戏', '', '', '', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('888', 'admin888', '吉祥急救网', 'www.lzsfas.org', '安全急救', 'http://sc.china.com.cn', '响应式（PC端改）', 'http://120.qm120.com', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('889', 'admin888', '萌宠时代', 'www.wootv.org', '宠物医院', 'http://www.izhufu.net', '响应式（PC端改）', 'http://www.docterpet.com/cwyy/', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('890', 'admin888', '风度汽车网', 'www.ysscdc.org', '汽车', '', '响应式（PC端改）', 'http://www.cvnews.com.cn/portal.php?mod=list&catid=27', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('891', 'admin888', '满面春风', 'www.mop360.net', '段子，笑话', 'https://www.ixinwei.com', '响应式（PC端改）', 'http://www.taohuaan.net/s/1.html', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('892', 'admin888', '花飞榭', 'www.teatickes.net', '花', '', '', '', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('893', 'admin888', '智慧科技', 'www.dacszp.com', '科技生活', '', '', 'https://www.iyiou.com/Rail_Transit/', '2019-09-07 09:30:07');
INSERT INTO `template` VALUES ('894', 'admin888', '全球电影新闻网', 'www.dnmyzs.com', '电影资讯', 'www.4hw.com.cn', '响应式（PC端改）', 'http://www.glofilm.com/film/huayu/', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('895', 'admin888', '净佛教', 'www.e-shyn.com', '佛学', 'https://www.infogame.cn', '响应式（PC端改）', 'http://www.gming.org/fxrm/fhjt/', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('896', 'admin888', '安心电商', 'www.huabeils.com', '电商', 'https://yule.youbian.com', 'https://m.youbian.com', 'http://www.dsb.cn', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('897', 'admin888', '方拓生物', 'www.sangebang.net', '生物技术', 'https://www.jkthin.com', '响应式（PC端改）', 'http://www.bio1000.com', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('898', 'admin888', '宇宙网', 'www.hzxmz.cn', '宇宙资讯', 'https://www.gq.com.cn', '响应式（PC端改）', 'https://space.qq.com/twtx.htm', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('899', 'admin888', '顺康健康网', 'www.yxtzkg.com', '健康', 'https://yangsheng.120ask.com', '响应式（PC端改）', 'https://www.jk3721.com/health', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('900', 'admin888', '幸福亲子', 'www.zzspread.com', '亲子资讯', 'www.yoyojiu.com', '响应式（PC端改）', 'https://www.mama.cn', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('901', 'admin888', '享受生活', 'www.jxhtjn.com', '生活', 'www.jiachong.net', '响应式（PC端改）', 'https://www.mshishang.com', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('902', 'admin888', '风度汽车网', 'www.ysscdc.org', '汽车', 'www.menworld.org', '响应式（PC端改）', 'http://www.cvnews.com.cn', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('903', 'admin888', '恋爱技巧网', 'www.ilistening.net', '恋爱', 'www.77lux.com', '响应式（PC端改）', 'https://www.puamap.com', '2019-09-07 09:30:08');
INSERT INTO `template` VALUES ('904', 'admin888', '科技探索', 'www.xiyangyang2.com', '探索科技', 'www.jp14.com', '响应式（PC端改）', 'http://www.tanmizhi.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('905', 'admin888', '天星风水', 'www.honglangdoors.com', '看风水', 'www.suanming.com.cn', '响应式（PC端改）', 'http://www.xinfanwan.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('906', 'admin888', '四季植物网', 'www.eeestyles.com', '植物', 'http://www.chinacaipu.com', '响应式（PC端改）', 'https://www.zw3e.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('907', 'admin888', '美硕酒业', 'www.taidiyingli.com', '红酒资讯', 'http://www.zyzydq.com', '响应式（PC端改）', 'https://www.putaojiu.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('908', 'admin888', '千帆漫画', 'www.chengbian.net', '漫画', 'http://www.meiyixia.com', 'http://m.meiyixia.com', 'http://www.pmjun.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('909', 'admin888', '音乐百科', 'www.chen-xi.net', '音乐看点', 'http://www.zswxy.cn', '响应式（PC端改）', 'http://www.mnw.cn', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('910', 'admin888', '百川教育', 'www.zly168.com', '教育', 'www.lieqidao.com', '响应式（PC端改）', 'http://www.rshgt.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('911', 'admin888', '学佛故事', 'www.zitie.org', '佛家故事', 'http://www.infogame.cn', '响应式（PC端改）', 'http://www.gming.org', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('912', 'admin888', '品尚农业', 'www.zhanyan.org', '农业资讯', '不详', '响应式（PC端改）', 'http://www.nyzy.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('913', 'admin888', '建筑土木网', 'www.biyefun.com', '土木资讯', 'https://www.souid.com', '响应式（PC端改）', 'https://www.xianjichina.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('914', 'admin888', '财经帮', 'www.liuzz.org', '理财', 'https://www.euroa.net', '响应式', 'http://fintech.xinhua08.com', '2019-09-07 09:30:09');
INSERT INTO `template` VALUES ('915', 'admin888', '电力资讯网', 'www.hnexue.org', '电力资讯', 'https://www.qianzhan.com', 'https://m.qianzhan.com', 'https://www.xianjichina.com/news', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('916', 'admin888', '玩游戏', 'www.wadh.org', '游戏资讯', 'https://www.dangdaigongyi.com.cn', 'https://m.dangdaigongyi.com.cn', 'https://http://www.mnw.cn', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('917', 'admin888', '民俗网', 'www.techface.org', '风俗，民俗', 'http://www.dnzs678.com/', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('918', 'admin888', '九宏家电', 'www.shenghai.org', '电器', '不详', '响应式（PC端改）', 'http://www.tidejd.com', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('919', 'admin888', '汽车资讯网', 'www.sinoicity.cn', '汽车', 'http://news.huahuo.com', 'http://m.huahuo.com', 'http://www.chebiaow.com', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('920', 'admin888', '唯情网', 'www.aichaojj.cn', '情感文章', '', 'http://www.manshijian.com', 'http://m.manshijian.com', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('921', 'admin888', '论历史', 'www.hy-elec.cn', '历史资讯', 'http://www.nanchongzol.com', 'http://m.nanchongzol.com', 'http://www.360changshi.com', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('922', 'admin888', 'M职场网', 'www.sdicdt.cn', '职场', 'http://www.faxingzhan.com', '响应式（PC端改）', 'http://www.360changshi.com', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('923', 'admin888', '电脑常识网', 'www.07ms.cn', '电脑常识', '不详', '响应式（PC端改）', 'http://www.dnzs678.com', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('924', 'admin888', '智慧电脑网', 'www.bbwqc.cn', '电脑', '不详', '响应式（PC端改）', 'http://www.dngsw.cn', '2019-09-07 09:30:10');
INSERT INTO `template` VALUES ('925', 'admin888', '时尚签名网', 'www.meixim.cn', '签名，头像', 'https://www.duwzw.com', '响应式（PC端改）', 'https://www.feizl.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('926', 'admin888', '暗夜手机网', 'www.pcuser.cn', '手机', 'http://www.jinlila.com', '响应式（PC端改）', 'http://www.cnmo.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('927', 'admin888', '纤姿雅', 'www.ckdmro.com', '减肥，肥胖', 'http://www.520730.com', 'http://m.520730.com', 'http://www.jianfei.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('928', 'admin888', '婚嫁攻略', 'www.gladsin.cn', '婚礼，结婚', 'http://www.dianping.com', 'http://m.dianping.com', 'https://www.hunmanyi.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('929', 'admin888', '时尚秀', 'www.questex.cn', '服装', 'http://www.mimito.com.cn', '响应式', 'http://www.hao528.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('930', 'admin888', '遇见情感网', 'www.ziti365.com', '情感资讯', 'http://www.zgsspw.com', 'http://m.zgsspw.com', 'http://www.hao528.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('931', 'admin888', '幸福窝', 'www.xinxinsolar.com', '家居', 'http://www.juziss.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('932', 'admin888', '装修资讯网', 'www.flconsulte.com', '装修资讯', 'http://www.qiguaiw.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('933', 'admin888', '装修施工网', 'www.tysysm.com', '装修施工', 'http://www.nuuha.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('934', 'admin888', '优质生活', 'www.bajieol.com', '生活', 'http://www.hunmanyi.com', 'http://m.hunmanyi.com', 'https://www.38fan.com', '2019-09-07 09:30:11');
INSERT INTO `template` VALUES ('935', 'admin888', '看好房', 'www.dxg89.com', '看房', 'http://www.xieshudeng.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('936', 'admin888', '买好房', 'www.q6u.com', '买房', 'http://www.q6u.com', '响应式（PC端改）', 'https://zhishi.fang.com', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('937', 'admin888', '最资讯', 'www.mfcad.com', '世界之最', 'www.mfcad.com', '响应式（PC端改）', 'https://www.souid.com/', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('938', 'admin888', '快科普', 'www.mfcad.com', '科普', '不详', '响应式（PC端改）', 'https://www.souid.com/', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('939', 'admin888', '潮鞋帮', 'www.fujitech-sh.com', '鞋子', 'http://www.jiushixing.com', 'http://m.jiushixing.com', 'http://www.q6u.com', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('940', 'admin888', '亲子宝贝', 'www.cszlg.com', '亲子', 'http://www.hao528.com', 'http://m.hao528.com', 'http://www.5h.com', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('941', 'admin888', '糖果娱乐', 'www.hbxingjian.com', '娱乐，明星', 'http://www.lamaying.com', 'http://m.lamaying.com', 'https://www.ixiumei.com', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('942', 'admin888', 'kiki私宠', 'www.atwoodmats.com', '宠物猫', 'http://qmjs.tiqiu.com', 'http://m.qmjs.tiqiu.com', 'http://www.boqii.com', '2019-09-07 09:30:12');
INSERT INTO `template` VALUES ('943', 'admin888', '水族之家', 'www.wiyaphotos.com', '水族', 'http://www.cosmofashion.com.cn', 'http://m.cosmofashion.com.cn', 'http://www.boqii.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('944', 'admin888', '绿缘植物', 'www.dualwarez.com', '植物', 'http://www.nfyl.net', 'http://m.nfyl.net', 'http://www.boqii.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('945', 'admin888', '普生医疗', '', '医疗', 'http://www.tz.ef360.com', 'http://m.tz.ef360.com', 'https://www.xianjichina.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('946', 'admin888', '爱尚摄影', '', '摄影', 'http://www.shunvzhi.com', 'http://m.shunvzhi.com', 'http://www.nitutu.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('947', 'admin888', '时尚数码', '', '数码', 'http://www.handanol.com', 'http://m.handanol.com', 'https://www.nanrenwo.net', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('948', 'admin888', '爱美容', '', '美容', 'http://www.faxingsj.com', 'http://m.faxingsj.com', 'http://www.meifajie.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('949', 'admin888', '东方影视', '', '影视', 'http://http://www.365j.com', 'http://m.365j.com', 'http://news.ylq.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('950', 'admin888', '西方影视', '', '影视', 'http://www.ylq.com', 'http://m.ylq.com', 'http://yule.youbian.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('951', 'admin888', '香吉士', 'www.jpsbzr.com', '香水', 'http://www.meiyanw.com', 'https://mip.nvren.com', 'https://www.nvren.com', '2019-09-07 09:30:13');
INSERT INTO `template` VALUES ('952', 'admin888', '丽人彩妆', '', '彩妆攻略', 'http://www.meirijinrong.com', 'http://m.meirijinrong.com', 'https://www.nvren.com', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('953', 'admin888', '悠悠美发', 'www.559464.com', '美发', 'http://www.movie.itedou.com', 'http://m.movie.itedou.com', 'https://www.eastlady.cn', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('954', 'admin888', '潮流搭配', '', '时尚搭配', 'http://www.women-health.cn', 'http://m.women-health.cn', 'https://www.eastlady.cn', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('955', 'admin888', '爱狗网', '未知', '宠物，狗', 'http://www.ichong123.com/', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('956', 'admin888', '小宠网', '未知', '小型宠物', 'www.bianzhirensheng.com', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('957', 'admin888', '爬行宠物网', '未知', '小型宠物 爬虫宠物 爬行宠物', '不详', '响应式（PC端改）', 'www.boqii.com', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('958', 'admin888', '小丑熊', '未知', '亲子', '不详', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('959', 'admin888', '健康生活', '未知', '生活', 'dc.pchome.net', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('960', 'admin888', '女性健康', '未知', '妇科 保健 健康', 'www.intfad.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:14');
INSERT INTO `template` VALUES ('961', 'admin888', '新派饮食', '未知', '美食', 'www.achongwu.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('962', 'admin888', '电子信息', '未知', '电子 信息技术', 'www.qinbaol.com', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('963', 'admin888', '机械工程', '未知', '机械 工程', 'http://news.szhk.com/', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('964', 'admin888', '化学化工', '未知', '化学 工程', 'ent.qianzhan.com', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('965', 'admin888', '媒体报道', '未知', '媒体 报道 资讯', 'http://yl.szhk.com/', '响应式（PC端改）', 'https://www.xianjichina.com/news/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('966', 'admin888', '人群健康', '未知', '人群 健康', 'www.meifajie.com', '响应式（PC端改）', 'http://www.360changshi.com/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('967', 'admin888', '今日百科', '未知', '百科 生活', 'www.hanjutv.com', '响应式（PC端改）', 'https://www.eastlady.cn/baike/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('968', 'admin888', '我爱文章网', '未知', '文章 好词好句 经典', 'www.pcoof.com', 'http://mip.duanmeiwen.com/lizhi/', 'http://www.jingdianwenzhang.cn/', '2019-09-07 09:30:15');
INSERT INTO `template` VALUES ('969', 'admin888', '悠悠减肥', '未知', '减肥 生活', 'www.aitaotu.com', '响应式（PC端改）', 'https://www.eastlady.cn/baike/', '2019-09-07 09:30:15');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `nick` varchar(10) NOT NULL,
  `time` datetime NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', '15897710445', '88888888', 'admin01', '2019-09-06 09:40:31', '../static/img/avatar-5d723a07e38b6.jpg', '2019-09-06', '2', '2');
INSERT INTO `users` VALUES ('5', 'admin888', '123456', 'admin', '2019-09-06 11:35:02', '../static/img/avatar-5d72efb678176.jpg', '2019-08-06', '0', '0');
