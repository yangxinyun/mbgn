﻿<?php

$user = $_SESSION["user"];

$query_avatar_gender = mysqli_query($connect, 'select * from users where nick="'.$user.'";');

if (!$query_avatar_gender) {

    header('Location:../tip.php');

}

$avatar_gender = array();

while ($item = mysqli_fetch_assoc($query_avatar_gender)) {

    $avatar_gender[] = $item;

}
if (empty($avatar_gender[0]["avatar"])) {

    if ($avatar_gender[0]["gender"]== 0) {

        $default_avatar = '../static/img/0.png';

    } else if ($avatar_gender[0]["gender"]== 1) {

        $default_avatar = '../static/img/1.png';

    } else if($avatar_gender[0]["gender"]== 2) {

        $default_avatar = '../static/img/2.png';

    }
}else{

    $default_avatar = $avatar_gender[0]["avatar"];

}

$aside = "col-sm-9 col-sm-offset-3 col-md-11 col-md-offset-1 main";

$domain = "http://localhost";

?>
<script src="../../static/lib/layui/layui.js"></script>
<link href="../../static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../../static/css/main.css" rel="stylesheet">
<style>.red{color:red}</style>
<header>
    <ul class="layui-nav">
        <li class="layui-nav-item">
            <a href="../index/index.php" style="font-size:18px;">模板归纳<span class="layui-badge">0</span></a>
        </li>
        <li style="float:right" class="layui-nav-item">
            <a href="<?php echo $domain; ?>/mbgn/views/user/user.php">个人中心<span class="layui-badge-dot"></span></a>
        </li>
        <dd style="float:right" class="layui-nav-item" >
            <a href="javascript:;"><img height="35" src="../<?php echo $default_avatar ?>">&nbsp;&nbsp;<?php echo $user; ?></a>
            <dl class="layui-nav-child">
                <dd><a href="<?php echo $domain; ?>/mbgn/views/user/password.php">修改密码</a></dd>
                <dd><a href="<?php echo $domain; ?>/mbgn/views/user/avatar.php">修改头像</a></dd>
                <dd><a href="<?php echo $domain; ?>/mbgn/views/msg/msglist.php">留言中心</a></dd>
                <dd><a href="<?php echo $domain; ?>/mbgn/index.php">切换账号</a></dd>
                <dd><a href="<?php echo $domain; ?>/mbgn/model/login/logout.php">退出登录</a></dd>
            </dl>
        </dd>
        <?php if (isset($search)) : ?>
        <form class="navbar-form navbar-right" action="index.php" method="post">
            <input type="text" name="username" id="search" class="form-control" placeholder="搜索...">
            <input type="submit" value="搜索" class="btn btn-default">
        </form>
        <?php endif ?>
    </ul>
</header>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-1 sidebar">
            <ul class="layui-nav layui-nav-tree layui-bg-cyan layui-inline" lay-filter="demo">
                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">模板管理</a>
                    <dl class="layui-nav-child">
                        <dd class="<?php if ($index) echo 'layui-this' ?>"><a href="<?php echo $domain; ?>/mbgn/views/index/index.php">信息列表</a></dd>
                        <dd class="<?php if ($index_add) echo 'layui-this' ?>"><a href="<?php echo $domain; ?>/mbgn/views/index/add.php">添加信息</a></dd>
                        <dd class="<?php if ($index_history) echo 'layui-this' ?>"><a href="<?php echo $domain; ?>/mbgn/views/history/history.php">历史记录</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">用户中心</a>
                    <dl class="layui-nav-child">
                        <dd class="<?php if ($user_index) echo 'layui-this' ?>"><a href="<?php echo $domain; ?>/mbgn/views/user/user.php">个人中心</a></dd>
                        <dd class="<?php if ($user_list) echo 'layui-this' ?>"><a href="<?php echo $domain; ?>/mbgn/views/user/userlist.php">用户列表</a></dd>
                        <dd class="<?php if ($user_message) echo 'layui-this' ?>"><a href="<?php echo $domain; ?>/mbgn/views/msg/msglist.php">留言中心</a></dd>
                    </dl>
                </li>
                <!-- <li class="layui-nav-item"><a href="<?php echo $domain; ?>/mbgn/message/msglist.php"></a></li> -->
            </ul>
        </div>
    </div>
</div>
<script>
    layui.use('element', function() {
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        var layer = layui.layer;
        //监听导航点击
        element.on('nav(demo)', function(elem) {
            //console.log(elem)
            // layer.msg(elem.text());
        });
    });
</script>