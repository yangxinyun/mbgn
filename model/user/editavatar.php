<?php

require('../../public/common.php');

checkLogin();

$id = $_POST['id'];

if (!empty($_FILES['avatar'])) {

    require("../../class/Upload.php");

    $upload = new Upload();

    $avatar = $upload->uploadFile('avatar');

    $avatar = str_replace('../../', '../', $avatar);

}

$query = mysqli_query($connect, "update users set avatar='{$avatar}' where id=" . $id . ";");

if (!$query) exit;

header('Location:../../views/user/avatar.php');
