<?php
require("../../public/common.php");

checkLogin();

if (empty($_GET["id"]))return;

$id = $_GET["id"];

$name = $_GET["name"];

$query_data = mysqli_query($connect, 'select name,title,domain,pc_url,keyword,content_url,mobile_url from template where id=' . $id . ';');

if (!$query_data)exit;

$data = mysqli_fetch_assoc($query_data);

$query_delete = mysqli_query($connect, 'delete from template where id=' . $id . ';');

if (!$query_delete) exit;

$time = date('Y-m-d H:i:s');

$ip = get_server_ip();

$query_history = mysqli_query($connect, "insert into history values(null,'{$ip}','{$data["name"]}','{$data["title"]}','{$data["domain"]}','{$data["keyword"]}','删除','{$data["pc_url"]}','{$data["mobile_url"]}','{$data["content_url"]}','{$time}');");

if (!$query_history)exit;

header("Location:../../views/index/index.php");
