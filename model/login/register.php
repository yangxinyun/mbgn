<?php
//表单进行了提交处理
session_start();
if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['repassword']) && !empty($_POST['nick']) && !empty($_POST['authcode'])) {

    require("../../public/common.php");

    $username = trim($_POST['username']);

    $nick = trim($_POST['nick']);

    $password = trim($_POST['password']);

    $repassword = trim($_POST['repassword']);

    $time = date('Y-m-d H:i:s');

    $authcode = $_POST['authcode'];

    $query = mysqli_query($connect, "select count(id) as id from users where username='{$username}';");

    if (!$query) {
        die(json_encode(array("code" => 4,"msg" => "操作数据库失败")));
    }
    $query_nick = mysqli_query($connect, "select count(id) as id from users where nick='{$nick}';");

    if (!$query_nick) {

        die(json_encode(array("code" => 4,"msg" => "操作数据库失败")));

    }

    $result = mysqli_fetch_assoc($query);

    $result1 = mysqli_fetch_assoc($query_nick);

    if (isset($result['id']) && $result['id'] > 0) {

        die(json_encode(array("code" => 1,"msg" => "用户名重复")));

    } elseif (isset($result1['id']) && $result1['id'] > 0) {

        die(json_encode(array("code" => 3,"msg" => "昵称重复")));
        
    } elseif (strtolower($authcode) != $_SESSION['authcode']) {

        die(json_encode(array("code" => 0,"msg" => "验证码错误")));

    } else {

        $query_ok = mysqli_query($connect, "insert into users values(null,'{$username}','{$password}','{$nick}','{$time}','../static/img/2.png','{$time}',2,null);");

        if (!$query_ok) {

            die(json_encode(array("code" => 4,"msg" => "服务器异常")));

        }

        die(json_encode(array("code" => 2,"msg" => "注册成功")));
    }
}
