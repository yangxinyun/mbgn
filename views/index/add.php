<?php

$index_add = 'active';

require("../../public/common.php");

checkLogin();

$username = 1;

?>
<!DOCTYPE html>

<html lang="zh-CN">

<head>
    <title>添加</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="../../static/lib/layui/css/layui.css">
</head>

<body>
    <?php require('../../public/layou.php'); ?>
    <div class="<?php echo $aside; ?>">
        <h2 class="sub-header">添加信息</h2>
        <form action="../../model/index/add.php" method="post">
            <div class="form-group">
                <label for="">姓名<span class="red"> *<span></label>
                <input type="text" class="form-control" id="" name="name" required minlength="2" maxlength="10" placeholder="必填，字数2-10位" value="<?php echo $user; ?>">
            </div>
            <div class="form-group">
                <label for="">名称<span class="red"> *<span></label>
                <input type="text" class="form-control" placeholder="必填，字数2-100位" id="" name="title" required minlength="2" maxlength="100">
            </div>
            <div class="form-group">
                <label for="">域名<span class="red"> *<span></label>
                <input class="form-control" type="text" id="" name="domain" maxlength="100" placeholder="必填，字数2-100位" required>
            </div>
            <div class="form-group">
                <label for="">关键字</label>
                <input class="form-control" type="text" id="" name="keyword" placeholder="选填，字数不多于100字" maxlength="100" >
            </div>
            <div class="form-group">
                <label for="">PC模板地址</label>
                <input class="form-control" type="text" id="" name="pc_url"  placeholder="选填，字数不多于100字" maxlength="100" >
            </div>
            <div class="form-group">
                <label for="">手机模板地址</label>
                <input class="form-control" type="text" id="" name="mobile_url" maxlength="100" placeholder="选填，字数不多于100字" >
            </div>
            <div class="form-group">
                <label for="">内容地址</label>
                <input class="form-control" type="text" id="" name="content_url" maxlength="100" placeholder="选填，字数不多于100字" >
            </div>
            <button type="submit" class="layui-btn layui-btn-normal">提交</button>
            <a href="index.php" class="layui-btn layui-btn-danger">取消</a>
        </form>
    </div>
</body>

</html>