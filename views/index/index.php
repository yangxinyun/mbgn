<?php

$index = 'active';

$search = 'search';

require("../../public/common.php");

require('../../class/Page.php');

checkLogin();

$data = array();

$query_count = mysqli_query($connect, 'select count(*) as count from template;');

if (!$query_count)exit();

require('../../public/indexPage.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $username = $_POST['username'];

    $query = mysqli_query($connect, "select * from template where name='{$username}' or domain like '%{$username}%' or title like '%{$username}%' or pc_url like '%{$username}%' or content_url like '%{$username}%';");

    if (!$query)exit();

    $data = array();

    while ($item = mysqli_fetch_assoc($query)) {
        $data[] = $item;
    }

    $query_count = mysqli_query($connect, "select count(*) as count from template where name='{$username}' or domain like '%{$username}%' or title like '%{$username}%';");

    if (!$query_count)exit();

    $count = (int) mysqli_fetch_assoc($query_count)["count"];

    
}
?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <title>首页</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="../../static/lib/layui/css/layui.css">
    <script src="../../static/lib/jquery/jquery.min.js"></script>
    <script src="../../static/lib/bootstrap/js/bootstrap.min.js"></script>
</head>

<body>
    <?php require('../../public/layou.php'); ?>
    <div class="<?php echo $aside; ?>">
        <h2 class="sub-header">信息列表</h2>
        <a class="layui-btn" href="add.php">添加信息</a>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>名称</th>
                        <th>域名(<?php echo $count ?>)</th>
                        <th>模板地址</th>
                        <th>内容地址</th>
                        <th>时间</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $student) : ?>
                    <?php ?>

                    <tr>
                        <td>
                            <?php echo $student['name'] ?>
                        </td>
                        <td>
                            <?php echo $student['title'] ?>
                        </td>
                        <td>
                        <?php str_replace('https://', '', $student['pc_url']); ?>
                            <?php echo "<a target='_blank' href=http://" . $student['domain'] . ">" . $student['domain'] . "</a>" ?>
                        </td>
                        <td>
                            <?php $student['pc_url']=str_replace('https://', '', $student['pc_url']); ?>
                            <?php echo "<a target='_blank' href='http://" . str_replace('http://', '', $student['pc_url']) . "'>" . $student['pc_url'] . "</a>" ?>
                        </td>
                        <td>
                        <?php $student['content_url']=str_replace('https://', '', $student['content_url']);?>
                            <?php echo "<a target='_blank' href='http://" . str_replace('http://', '', $student['content_url']). "'>" . $student['content_url'] . "</a>" ?>
                        </td>
                        <td>
                            <?php echo $student['time'] ?>
                        </td>
                        <td>
                            <a class="layui-btn" href="../../views/index/edit.php?id=<?php echo $student['id'] ?>">编辑</a>
                            <?php $url="../../model/index/delete.php?id=" . $student['id'] . '&name=' . $user ?>
                            <a class="layui-btn layui-btn-danger" onclick="delcfm('<?php echo $url ?>')">删除</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                    <?php if (count($data) == 0) {
                        echo "<tr><td style='text-align:center;color:#999' colspan='6'>什么都没有...<td><tr>";
                    } ?>
                </tbody>
            </table>
            <?php if ($_SERVER['REQUEST_METHOD'] != 'POST') : ?>
            <nav>
                <ul class="pagination text-center">
                    <li><a href="<?php echo $domain; ?>/mbgn/index/index.php" aria-label="Previous"><span aria-hidden="true">首页</span></a></li>
                    <li><a href="<?php echo $prev ?>" aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>
                    <?php for ($i = 1; $i <= $page->totalPage; $i++)
                            if ($currentpage == $i) {
                                echo "<li class='active' href=''><a>" . $i . "</a></li>";
                            } else {
                                echo "<li><a href='{$domain}/mbgn/views/index/index.php?page={$i}'>" . $i . "</a></li>";
                            }
                        ?>
                    <li>
                        <a href="<?php echo $next ?>" aria-label="Next">
                            <span aria-hidden="true ">下一页</span>
                        </a>
                    </li>
                    <?php echo "<li><a href='{$domain}/mbgn/index/index.php?page={$page->totalPage}'>尾页</a></li>";  ?>
                </ul>
            </nav>
            <?php endif ?>
            <div class="modal fade" id="delcfmModel">
                <div class="modal-dialog">
                    <div class="modal-content message_align">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">提示信息</h4>
                        </div>
                        <div class="modal-body">
                            <p>您确认要删除吗？删除后将无法恢复！</p>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="url" />
                            <a onclick="urlSubmit()" class="layui-btn layui-btn-danger" data-dismiss="modal">确定</a>
                            <button type="button" class="layui-btn layui-btn-info" data-dismiss="modal">取消</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function delcfm(url) {
            $('#url').val(url); 
            $('#delcfmModel').modal();
        }

        function urlSubmit() {
            var url = $.trim($("#url").val()); 
            window.location.href = url;
        }
    </script>
</body>

</html>