<?php

$index_edit = 'active';

require("../../public/common.php");

checkLogin();

$username = 1;

if (empty($_GET["id"]))return;

$id = $_GET["id"];

$query = mysqli_query($connect, "select * from template where id=" . $id . ';');

if (!$query) exit;

while ($item = mysqli_fetch_assoc($query)) {
    $data = $item;
}

?>
<!DOCTYPE html>

<html lang="zh-CN">

<head>
    <title>编辑</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="../../static/lib/layui/css/layui.css">
</head>

<body>
    <?php require('../../public/layou.php'); ?>
    <div class="<?php echo $aside; ?>">
        <h2 class="sub-header">编辑信息</h2>
        <form action="../../model/index/edit.php" method="post">
            <input type="hidden" name="id" value="<?php echo $id ?>">
            <div class="form-group">
                <label for="">姓名<span class="red"> *<span></label>
                <input type="text" class="form-control" id="" name="name" required minlength="2" maxlength="10" value="<?php echo $user; ?>"/>
            </div>
            <div class="form-group">
                <label for="">名称<span class="red"> *<span></label>
                <input type="text" class="form-control" id="" name="title" required minlength="2" maxlength="100" value="<?php echo $data['title']; ?>"/>
            </div>
            <div class="form-group">
                <label for="">域名<span class="red"> *<span></label>
                <input class="form-control" type="text" id="" name="domain" maxlength="100" required value="<?php echo $data['domain']; ?>"/>
            </div>
            <div class="form-group">
                <label for="">关键字</label>
                <input class="form-control" type="text" id="" name="keyword" maxlength="100" value="<?php echo $data['keyword']; ?>"/>
            </div>
            <div class="form-group">
                <label for="">PC模板地址</label>
                <input class="form-control" type="text" id="" name="pc_url" maxlength="100" value="<?php echo $data['pc_url']; ?>"/>
            </div>
            <div class="form-group">
                <label for="">手机模板地址</label>
                <input class="form-control" type="text" id="" name="mobile_url" maxlength="100" value="<?php echo $data['mobile_url']; ?>"/>
            </div>
            <div class="form-group">
                <label for="">内容地址</label>
                <input class="form-control" type="text" id="" name="content_url" maxlength="100" value="<?php echo $data['content_url']; ?>"/>
            </div>
            <button type="submit" class="layui-btn layui-btn-normal">修改</button>
            <a href="index.php" class="layui-btn layui-btn-danger">取消</a>
        </form>
    </div>
</body>

</html>