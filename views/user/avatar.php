<?php

$user_edit_avatar = 'active';

require('../../public/common.php');

checkLogin();

$data = array();

$name = $_SESSION['user'];

$query = mysqli_query($connect, "select * from users where nick='" . $name . "';");

if (!$query) exit();

while ($item = mysqli_fetch_assoc($query)) {
    $data = $item;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>个人中心</title>
    <link rel="stylesheet" href="../../static/lib/layui/css/layui.css">
</head>

<body>
    <?php require('../../public/layou.php'); ?>
    <div class="<?php echo $aside; ?>">
        <h2 class="sub-header">修改头像</h2>
        <form method="post" action="../../model/user/editavatar.php" id="formdata" enctype="multipart/form-data">
            <input type="hidden" name="id" id="id" value="<?php echo $data['id'] ?>">
            <div class="form-group">
                <img id="big" style="margin-bottom:20px" height="400" src="../<?php echo $default_avatar; ?>" />
                <input type="file" style="display: none" class="form-control" accept="image/jpeg" id="avatar" name="avatar">
                <div class="input-append">
                    <input id="showname" class="input-large" type="text" disabled style="height:30px;width:350px">
                    <a class="btn btn-primary btn-sm" onclick="makeThisfile()" id="browse">浏览</a>
                </div>
            </div>
            <button id="submit" type="submit" class="layui-btn layui-btn-normal">上传</button>
            <a href="../../views/user/user.php" class="layui-btn layui-btn-danger">返回</a>
        </form>
    </div>

    </div>
    </div>
    <script src="../../static/lib/jquery/jquery.min.js"></script>
    <script src="../../static/lib/layui/layui.all.js"></script>
    <script type="text/javascript">
        function makeThisfile() {
            $('#avatar').click();
        }
        $('#avatar').change(function() {
            $('#showname').val($(this).val())
        })
    </script>
</body>

</html>