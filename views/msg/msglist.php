<?php

$user_message = 'active';

require("../../public/common.php");

checkLogin();

require('../../model/msg/index.php');

?>
<!DOCTYPE html>

<html lang="zh-CN">

<head>
    <title>留言中心</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="../../static/lib/layui/css/layui.css">
</head>

<body>
    <?php require('../../public/layou.php'); ?>
    <div class="<?php echo $aside; ?>">
        <form>
            <h2 class="sub-header">给我留言</h2>
            <textarea name="msg" id="txt" class="form-control" cols="30" rows="8" placeholder="请输入留言内容，字数在5-100字之间"></textarea>
            <input type="hidden" name="nick" id="nick" value="<?php echo $user; ?>" />
            <button style="margin-top:10px" type="button" id="submit" class="layui-btn layui-btn-normal pull-right">发布</button>
        </form>
    </div>
    <div class="<?php echo $aside; ?>">
        <?php foreach ($data as $msg) : ?>
            <div class="list">
                <div class="col-sm-1 col-md-1"><img src="../<?php echo $msg['avatar']; ?>" height="60" />
                    <p class="nick"><?php echo $msg['nick']; ?></p>
                </div>
                <div class="col-sm-11 col-md-11">
                    <p class="msg"><?php echo $msg['msg']; ?><p>
                     <p class="time"><?php echo $msg['time']; ?><p>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    </div>
    </div>
    <script src="../../static/lib/jquery/jquery.min.js"></script>
    <script src="../../static/lib/layui/layui.all.js"></script>
    <script>
        $(function() {
            $("#submit").click(function() {
                if ($("#txt").val() == '' || $('#txt').val().length <= 0) {
                    layer.tips('内容不能为空！', '#txt', {
                        time: 2000,
                        tips: 2
                    });
                } else if ($("#txt").val().length > 100 || $('#txt').val().length < 5) {
                    layer.tips('内容长度必须是5-100个字符！', '#txt', {
                        time: 2000,
                        tips: 2
                    });
                } else {
                    $.ajax({
                        type: "POST",
                        url: '../../model/msg/add.php',
                        data: {
                            nick: $("#nick").val(),
                            msg: $("#txt").val()
                        },
                        success: function(res) {
                            var data = JSON.parse(res);
                            if (data.code == 0) {
                                window.location = "msglist.php"
                            } else if (data.code == 1) {
                                layer.tips('昵称不能为空!', '#txt', {
                                    time: 2000,
                                    tips: 2
                                });
                            } else if (data.code == 2) {
                                layer.tips('内容不能为空!', '#txt', {
                                    time: 2000,
                                    tips: 2
                                });
                            } else if (data.code == 3) {
                                layer.tips('昵称必须是2-10个字符串', '#txt', {
                                    time: 2000,
                                    tips: 2
                                });
                            } else if (data.code == 4) {
                                layer.tips('内容长度必须是5-100个字符', '#txt', {
                                    time: 2000,
                                    tips: 2
                                });
                            } else if (data.code == 5) {
                                layer.alert('服务器异常', {
                                    icon: 6
                                });
                            }
                        }
                    })
                }
            })
        })
    </script>
</body>

</html>