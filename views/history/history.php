<?php

require("../../public/common.php");

$index_history = 1;

checkLogin();

require('../../class/Page.php');

$query = mysqli_query($connect, 'select * from history');

if (!$query)exit;

$query_count = mysqli_query($connect, 'select count(*) as count from history;');

if (!$query_count)exit;

require('../../public/historyPage.php');

?>
<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <title>首页</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../../static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../static/lib/layui/css/layui.css">
    <link href="../../static/css/main.css" rel="stylesheet">
</head>

<body>
    <?php require('../../public/layou.php'); ?>
    <div class="<?php echo $aside; ?>">
        <h2 class="sub-header">历史记录</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ip</th>
                        <th>昵称</th>
                        <th>操作</th>
                        <th>名称</th>
                        <th>域名</th>
                        <th>关键字</th>
                        <th>PC模板地址</th>
                        <th>手机模板地址</th>
                        <th>内容地址</th>
                        <th>时间</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($data as $history) : ?>
                        <tr>
                            <td>
                                <?php echo $history['ip'] ?>
                            </td>
                            <td>
                                <?php echo $history['nick'] ?>
                            </td>
                            <td>
                                <?php echo $history['crud'] ?>
                            </td>
                            <td>
                                <?php echo $history['title'] ?>
                            </td>
                            <td>
                                <?php echo "<a target='_blank' href=http://" . str_replace('http://', '', $history['domain']) . ">" . $history['domain'] . "</a>" ?>
                            </td>
                            <td>
                                <?php echo $history['keyword'] ?>
                            </td>
                            <td>
                                <?php echo "<a target='_blank'  href=http://" . str_replace('http://', '', $history['pc_url']) .">" . $history['pc_url'] . "</a>" ?>
                            </td>
                            <td>
                                <?php echo "<a target='_blank' href=http://"  . str_replace('http://', '', $history['mobile_url']) . ">" . $history['mobile_url'] . "</a>" ?>
                            </td>
                            <td>
                                <?php echo "<a target='_blank'  href=http://" . str_replace('http://', '', $history['content_url']) . ">" . $history['content_url'] . "</a>" ?>
                            </td>
                            <td>
                                <?php echo $history['time'] ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    <?php if (count($data) == 0) {
                        echo "<tr><td style='text-align:center;color:#999' colspan='6'>什么都没有...<td><tr>";
                    } ?>
                </tbody>
            </table>
            <?php if ($_SERVER['REQUEST_METHOD'] != 'POST') : ?>
                <nav>
                    <ul class="pagination text-center">
                        <li><a href="<?php echo $prev ?>" aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>
                        <?php for ($i = 1; $i <= $page->totalPage; $i++)
                            if ($currentpage == $i) {
                                echo "<li class='active' href=''><a>" . $i . "</a></li>";
                            } else {
                                echo "<li><a href='{$domain}/mbgn/views/history/history.php?page={$i}'>" . $i . "</a></li>";
                            }
                        ?>
                        <li>
                            <a href="<?php echo $next ?>" aria-label="Next">
                                <span aria-hidden="true ">下一页</span>
                            </a>
                        </li>
                        <li><a><span aria-hidden="true">总共<?php echo $count; ?>条记录</span></a></li>
                    </ul>
                </nav>
            <?php endif ?>
        </div>
    </body>
</html>